﻿using System.CodeDom;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;
using Remark.Controller;



namespace Remark.Cleaner
{
    public class Cleaner
    {
        /// <summary>
        /// constructor of a Remark cleaner
        /// </summary>
        public Cleaner()
        {
            _timer.Elapsed += timer_Elapsed;
            _lock = new Locker();
            _writerReader = new RemarkXmlWriterReader();
        }

       
        /// <summary>
        /// this commands cleans the drive.
        /// </summary>
        public void Clean()
        {
            try
            {
                if (!System.IO.Directory.Exists(Directory)) return;

                //lock the directory and check if it is locked
                _lock.DirectoryToLock = Directory;
                _lock.Lock();
                if (!_lock.Locked) return;

                //get all the single remarks;
                var files = System.IO.Directory.GetFiles(Directory, "Remark*.XML");

                //only do this if there are things to do.
                if (files.Length > 0)
                {
                    var remarksfromfiles = new ObservableCollection<Controller.Remark>();

                    foreach (var file in files)
                    {
                        remarksfromfiles.Add(_writerReader.ReadSingleRemark(file));
                    }

                    //get all the files in the mainfile
                    var Mainremarks =
                        _writerReader.ReadAllRemarks(Path.Combine(Directory, Controller.RemarkControler.FileName)) ??
                        new ObservableCollection<Controller.Remark>();

                    //merge the files if they exists
                    foreach (var remark in remarksfromfiles)
                    {
                        if (!Mainremarks.Contains(remark)) Mainremarks.Add(remark);
                    }


                    //write the main file.
                    _writerReader.WriteAllRemarks(Mainremarks,
                        Path.Combine(Directory, Controller.RemarkControler.FileName));

                    //delete the other files only if the above worked out well.
                    foreach (var file in files)
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }
            
            finally
            {
                //unlock.
                _lock.Unlock();
            }


        }
        
        /// <summary>
        /// sets the timeinterval when a clean should be perfomed
        /// </summary>
        public int TimeOut
        {
            set { _timer.Interval = value; }
        }


        /// <summary>
        /// set if the cleaning should be enabled
        /// </summary>
        public bool Enable
        {
            set { _timer.Enabled = value; }
        }


        /// <summary>
        /// sets the directory where the remarks are stored.
        /// </summary>
        public string Directory { get; set; }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            Clean();
            _timer.Start();
        }
        private ILocker _lock;
        private IWriterReader _writerReader;
        private readonly Timer _timer = new Timer();


    }
}
