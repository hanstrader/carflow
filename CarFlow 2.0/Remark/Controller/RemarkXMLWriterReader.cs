﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Remark.Controller
{
    internal class RemarkXmlWriterReader : IWriterReader
    {
        private const int Retry = 10;
        private const int WaitTime = 100;


        #region Read Single Remark
        public Remark ReadSingleRemark(string file)
        {
            // write with a retry build in ... 
            var retryamount = Retry;
            var retrytime = WaitTime;
            var temp = new Remark();


            while (true)
            {
                try
                {
                    temp = ReadSingleRemarkXml(file);
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }

            return temp;
        }

        public async Task<Remark> ReadSingleRemarkAsync(string file)
        {
            var taak = Task.Factory.StartNew((() => ReadSingleRemark(file)));
            await taak;

            return taak.Result;
        }

        private Remark ReadSingleRemarkXml(string file)
        {
            if (!File.Exists(file))
            {
                return null;
            }

            var remark = new Remark();

            using (var filestream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
            using (var reader = XmlReader.Create(filestream))
            {
                while (reader.Read())
                {
                    if (!reader.IsStartElement()) continue;

                    switch (reader.Name)
                    {
                        case "Project":
                            remark.Project = reader.Read() ? reader.Value : "";
                            break;

                        case "Process":
                            remark.Process = reader.Read() ? reader.Value : "";
                            break;

                        case "Test":
                            remark.Test = reader.Read() ? reader.Value : "";
                            break;

                        case "FaultCode":
                            remark.Fault = reader.Read() ? reader.Value : "";
                            break;

                        case "Description":
                            remark.Description = reader.Read() ? reader.Value : "";
                            break;

                        case "Comment":
                            remark.Comment = reader.Read() ? reader.Value : "";
                            break;

                        case "VQDC":
                            remark.VQDC = reader.Read() ? reader.Value : "";
                            break;

                        case "SBS":
                            remark.SBS = reader.Read() ? reader.Value : "";
                            break;
                    }
                }
            }

            return remark;
        } 
        #endregion

        #region Read all remarks

        private ObservableCollection<Remark> ReadAllRemarksXML(string file)
        {
            if (!File.Exists(file))
            {
                return null;
            }

            var tempnew = new ObservableCollection<Remark>();
            var remark = new Remark();


            using (var filestream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
            using (var reader = XmlReader.Create(filestream))
            {
                while (reader.Read())
                {
                    if (!reader.IsStartElement()) continue;

                    switch (reader.Name)
                    {
                        case "Remark":
                            
                            remark = new Remark();
                            break;

                        case "Project":
                            remark.Project = reader.Read() ? reader.Value : "";
                            break;

                        case "Process":
                            remark.Process = reader.Read() ? reader.Value : "";
                            break;

                        case "Test":
                            remark.Test = reader.Read() ? reader.Value : "";
                            break;

                        case "FaultCode":
                            remark.Fault = reader.Read() ? reader.Value : "";
                            break;

                        case "Description":
                            remark.Description = reader.Read() ? reader.Value : "";
                            break;

                        case "Comment":
                            remark.Comment = reader.Read() ? reader.Value : "";
                            break;

                        case "VQDC":
                            remark.VQDC = reader.Read() ? reader.Value : "";
                            break;

                        case "SBS":
                            remark.SBS = reader.Read() ? reader.Value : "";

                            //add the remark to the list
                            if (!(remark.Project == null && remark.Process == null && remark.Test == null &&
                                  remark.Fault == null))
                            {
                                tempnew.Add(remark);
                            }

                            break;
                    }
                }
            }

            return tempnew.Count == 0 ? null : tempnew;
        }

        public ObservableCollection<Remark> ReadAllRemarks(string file)
        {
            // write with a retry build in ... 
            var retryamount = Retry;
            var retrytime = WaitTime;
            var temp = new ObservableCollection<Remark>();


            while (true)
            {
                try
                {
                    temp = ReadAllRemarksXML(file);
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }

            return temp;
        }

        public async Task<ObservableCollection<Remark>> ReadAllRemarksAsync(string file)
        {
            var taak = Task.Factory.StartNew(() => ReadAllRemarks(file));
            await taak;

            return taak.Result;
        }

        #endregion

        #region Write All Remarks

        public void WriteAllRemarks(ObservableCollection<Remark> remarks, string filename)
        {
            // write with a retry build in ... 
            var retryamount = Retry;
            var retrytime = WaitTime;


            while (true)
            {
                try
                {
                    WriteAllRemarksXml(remarks, filename);
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }
        }

        public async void WriteAllRemarksAsync(ObservableCollection<Remark> remarks, string filename)
        {
            var taak = Task.Factory.StartNew(() => WriteAllRemarks(remarks, filename));
            await taak;
        }

        private void WriteAllRemarksXml(ObservableCollection<Remark> remarks, string filename)
        {
            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = true
            };


            using (var filestream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            using (var writer = XmlWriter.Create(filestream, settings))
            {
                writer.WriteStartDocument();

                writer.WriteStartElement("Remarks");

                foreach (var remark in remarks)
                {
                    writer.WriteStartElement("Remark");

                    writer.WriteElementString("Project", remark.Project);
                    writer.WriteElementString("process", remark.Process);
                    writer.WriteElementString("Test", remark.Test);
                    writer.WriteElementString("FaultCode", remark.Fault);
                    writer.WriteElementString("Description", remark.Description);
                    writer.WriteElementString("Comment", remark.Comment);
                    writer.WriteElementString("VQDC", remark.VQDC);
                    writer.WriteElementString("SBS", remark.SBS);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        #endregion

        #region Write Single Remark

        public void WriteSingleRemark(Remark remark, string location)
        {
            // write with a retry build in ... 
            var retryamount = Retry;
            var retrytime = WaitTime;


            while (true)
            {
                try
                {
                    WriteSingleRemarkXml(remark, location);
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }
        }

        public async void WriteSingleRemarkAsync(Remark remark, string location)
        {
            var taak = Task.Factory.StartNew(() => WriteSingleRemarkXml(remark, location));
            await taak;
        }

        private void WriteSingleRemarkXml(Remark remark, string location)
        {
            var filename = Path.Combine(location, GetFileNameOutOfRemark(remark));

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = true
            };


            using (var filestream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            using (var writer = XmlWriter.Create(filestream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Remark");

                writer.WriteElementString("Project", remark.Project);
                writer.WriteElementString("process", remark.Process);
                writer.WriteElementString("Test", remark.Test);
                writer.WriteElementString("FaultCode", remark.Fault);
                writer.WriteElementString("Description", remark.Description);
                writer.WriteElementString("Comment", remark.Comment);
                writer.WriteElementString("VQDC", remark.VQDC);
                writer.WriteElementString("SBS", remark.SBS);

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        private string GetFileNameOutOfRemark(Remark remark)
        {
            var name = "Remark_" +
                       remark.Project + "_" +
                       remark.Process + "_" +
                       remark.Test + "_" +
                       remark.Fault + ".XML";

            return name;
        }

        #endregion
    }
}