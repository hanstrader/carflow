﻿namespace Remark.Controller
{
    public class Remark
    {
        public string Project { get; set; }
        public string Process { get; set; }
        public string Test { get; set; }
        public string Fault { get; set; }
        public string Description { get; set; }

        public string Comment { get; set; }
        public string VQDC { get; set; }
        public string SBS { get; set; }

        public override bool Equals(object obj)
        {
            //if parameter is null return false;
            if (obj == null) return false;

            //if parameter can not be cast to remark return null
            Remark opmerking = obj as Remark;
            if ((object) opmerking == null) return false;

            //if object is the same return true;
            return this.Project == opmerking.Project && this.Process == opmerking.Process && this.Test == opmerking.Test &&
                   this.Fault == opmerking.Fault;
        }

        public bool Equals(Remark obj)
        {
            //if parameter is null return false;
            if (obj == null) return false;

            //if object is the same return true;
            return this.Project == obj.Project && this.Process == obj.Process && this.Test == obj.Test &&
                   this.Fault == obj.Fault;
        }

        public override int GetHashCode()
        {
            return 999;
        }
    }
}
