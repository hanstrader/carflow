﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Remark.Controller
{
    interface IWriterReader
    {
        ObservableCollection<Remark> ReadAllRemarks(string file);
        Task<ObservableCollection<Remark>> ReadAllRemarksAsync(string file);

        Remark ReadSingleRemark(string file);
        Task<Remark> ReadSingleRemarkAsync(string file);

        void WriteSingleRemark(Remark remark,string location);
        void WriteSingleRemarkAsync(Remark remark, string location);

        void WriteAllRemarks(ObservableCollection<Remark> remarks, string filename);
        void WriteAllRemarksAsync(ObservableCollection<Remark> remarks, string filename);

    }
}
