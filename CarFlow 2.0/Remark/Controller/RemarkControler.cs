﻿using System.Collections.ObjectModel;
using System.IO;

namespace Remark.Controller
{
    public class RemarkControler
    {
        internal const string FileName = "CarFlowCarRemarks.xml";

        /// <summary>
        /// List holding the car remarks.
        /// </summary>
        public ObservableCollection<Remark> RemaksObservableCollection = new ObservableCollection<Remark>();

        /// <summary>
        /// constructor 
        /// </summary>
        public RemarkControler()
        {
            _writerReader = new RemarkXmlWriterReader();

            _watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime;
            _watcher.Filter = FileName;

            _watcher.Changed += WatcherChanged;
            _watcher.Error += WatcherError;
        }


        /// <summary>
        /// location where the file should be stored.
        /// </summary>
        public string FileLocation
        {
            get { return _fileLocation; }
            set
            {
                if (Directory.Exists(value))
                {
                    _fileLocation = value;

                    _watcher.Path = value;
                    _watcher.EnableRaisingEvents = true;

                    RemaksObservableCollection = _writerReader.ReadAllRemarksAsync(Path.Combine(value, FileName)).Result;
                }
            }
        }

        /// <summary>
        /// write a new remark on the drive.
        /// </summary>
        /// <param name="remark"></param>
        public void Write(Remark remark)
        {
            _writerReader.WriteSingleRemark(remark,FileLocation);
        }

        private void WatcherError(object sender, ErrorEventArgs e)
        {
            _watcher.EnableRaisingEvents = false;
        }
        private void WatcherChanged(object sender, FileSystemEventArgs e)
        {
            var file = Path.Combine(FileLocation, FileName);
            RemaksObservableCollection = _writerReader.ReadAllRemarksAsync(file).Result;
        }

        private readonly IWriterReader _writerReader;
        private readonly FileSystemWatcher _watcher = new FileSystemWatcher();
        private string _fileLocation;
    }
}
