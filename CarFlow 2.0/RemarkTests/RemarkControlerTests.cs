﻿using System;
using System.Threading;
using CarFlow.Remarks.Cleaner;
using CarFlow.Remarks.Controller;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RemarkTests
{
    [TestClass()]
    public class RemarkControlerTests
    {
        [TestMethod()]
        public void RemarkControlerTest()
        {
            var controller = new RemarkControler();
            controller.FileLocation = controller.FileLocation = AppDomain.CurrentDomain.BaseDirectory;

            var remark1 = new Remark()
            {
                Comment = "commentaar",
                Description = "bescrijving",
                Fault = "fout",
                Process = "process",
                Project = "Project1",
                SBS = "station by station",
                Test = "testje"

            };

            var remark2 = new Remark()
            {
                Comment = "commentaar",
                Description = "bescrijving",
                Fault = "fout",
                Process = "process",
                Project = "Project2",
                SBS = "station by station",
                Test = "testje"

            };

            var remark3 = new Remark()
            {
                Comment = "commentaar",
                Description = "bescrijving",
                Fault = "fout",
                Process = "process",
                Project = "Project3",
                SBS = "station by station",
                Test = "testje"

            };

            controller.Write(remark1);
            controller.Write(remark2);
            controller.Write(remark3);

            Cleaner clean = new Cleaner();
            clean.TimeOut = 2000;
            clean.Enable = true;
            clean.Directory = controller.FileLocation;
            clean.Clean();

            Thread.Sleep(5000);
        }


    }
}
