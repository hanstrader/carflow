﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CarFlow.MainWorker;
using CarFlow.Xlist;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SOLIDQBAY;

namespace CarFlow.Xlist.Tests
{
    [TestClass()]
    public class XListTests
    {
        private CancellationTokenSource source = new CancellationTokenSource();

        [TestMethod()]
        public void AllRunsTest()
        {
            QbayPlant.Plant = "Gent";
            var token = source.Token;

            var watch = new Stopwatch();
            watch.Start();

            var wagens = new List<string>() {"3584781", "3584772", "3584760", "3584753"};
            //var wagens = new List<string>() { "3580143" };

            Carflow werker = new Carflow();
            werker.GetDataAsync(wagens);

            var xlist = werker.AllRunMatrix;
            var ylist = werker.AllRemainingIssuesMatrix;
            var zlist = werker.FirstRunMatrix;

            watch.Stop();
            Trace.WriteLine(watch.ElapsedMilliseconds + " ms needed ");

            var t = xlist;
            var y = ylist;
            var z = zlist;

        }
    }
}
