﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Policy;
using System.Threading;
using CarFlow.MainWorker;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SOLIDQBAY;

namespace CarFlowTests.DataCollector
{
    [TestClass()]
    public class CollectorTests
    {
        private System.Threading.CancellationTokenSource source = new CancellationTokenSource();

        [TestMethod()]
        public  void GetAsyncTest()
        {
            QbayPlant.Plant = "Gent";

            var token = source.Token;
            var timer = new Stopwatch();

            timer.Start();
            //var wagens = new List<string>() {"3575526", "3575466", "3575403"};
            var wagens = new List<string>() { "3813539" };
            
            Carflow werker = new Carflow();
            werker.GetDataAsync(wagens);

            timer.Stop();
            Trace.Write(timer.ElapsedMilliseconds);

        }

        [TestMethod()]
        public void GetCarData()
        {
            QbayPlant.Plant = "Gent";
            var wagens = new List<string>() { "3813539" };

            Carflow werker = new Carflow();
            werker.GetData(wagens,false,false,false);

        }




    }
}
