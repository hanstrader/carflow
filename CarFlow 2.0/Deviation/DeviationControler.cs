﻿using System.Collections.ObjectModel;
using System.IO;


#pragma warning disable 169

namespace Carflow
{
    public class DeviationControler
    {
        internal const string FileName = "CarFlowDeviaties.xml";

        public DeviationControler()
        {
            //inject the readerwriter
            _writerReader = new DeviationXmlWriterReader();

            //set up the filesystemwatcher
            _watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime;
            _watcher.Filter = FileName;
            _watcher.Changed += WatcherChanged;
            _watcher.Error += WatcherError;
        }

        /// <summary>
        /// the collection holding the deviations
        /// </summary>
        public ObservableCollection<Deviatie> DeviatiesObservableCollection = new ObservableCollection<Deviatie>();
        
        /// <summary>
        /// the drive where the deviations will be located.
        /// </summary>
        public string FileLocation
        {
            get { return _fileLocation; }
            set
            {
                if (Directory.Exists(value))
                {
                    _fileLocation = value;

                    _watcher.Path = value;
                    _watcher.EnableRaisingEvents = true;

                    DeviatiesObservableCollection =   _writerReader.ReadAsync(Path.Combine(value, FileName)).Result;
                }
            }
        }
        

        /// <summary>
        /// method updates the deviation on the drive.
        /// </summary>
        public void UpdateDeviaties()
        {
            if (FileLocation == null) return;

            _writerReader.WriteAsync(Path.Combine(FileLocation, FileName), DeviatiesObservableCollection);
        }

        private void WatcherChanged(object sender, FileSystemEventArgs e)
        {
            DeviatiesObservableCollection = _writerReader.ReadAsync(Path.Combine(FileLocation, FileName)).Result;
        }
        
        private void WatcherError(object sender, ErrorEventArgs e)
        {
            _watcher.EnableRaisingEvents = false;
        }

        private readonly FileSystemWatcher _watcher = new FileSystemWatcher();
        
        private readonly IWriterReader _writerReader;
        
        private string _fileLocation;
    }
}