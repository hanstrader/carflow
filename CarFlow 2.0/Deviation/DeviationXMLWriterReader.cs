﻿using System;
using System.CodeDom.Compiler;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Carflow
{
    internal class DeviationXmlWriterReader : IWriterReader
    {
        private const int Retry = 10;
        private const int WaitTime = 100;


        public async Task<ObservableCollection<Deviatie>> ReadAsync(string file)
        {
            var taak = Task.Factory.StartNew(() => Read(file));

            await taak;

            return taak.Result.Count == 0? null : taak.Result;
        }
        public ObservableCollection<Deviatie> Read(string file)
        {
            // write with a retry build in ... 
            var retryamount = Retry;
            var retrytime = WaitTime;
            var temp = new ObservableCollection<Deviatie>();


            while (true)
            {
                try
                {
                    temp = ReadXml(file);
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }


            return temp.Count == 0? null : temp;
        }
        private ObservableCollection<Deviatie> ReadXml(string file)
        {
            if (!File.Exists(file))
            {
                return null;
            }

            var tempnew = new ObservableCollection<Deviatie>();
            var deviatie = new Deviatie();


            using (var filestream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
            using (var reader = XmlReader.Create(filestream))
            {
                while (reader.Read())
                {
                    if (!reader.IsStartElement()) continue;

                    switch (reader.Name)
                    {
                        case "Deviatie":
                            
                            deviatie = new Deviatie();
                            break;

                        case "Project":
                            deviatie.Project = reader.Read() ? reader.Value : "";
                            break;

                        case "Module":
                            deviatie.Module = reader.Read() ? reader.Value : "";
                            break;

                        case "Faultcode":
                            deviatie.FaultCode = reader.Read() ? reader.Value : "";
                            break;

                        case "Comment":
                            deviatie.Comment = reader.Read() ? reader.Value : "";

                            //store the deviation
                            if (
                                !(deviatie.Project == null && deviatie.Comment == null && deviatie.FaultCode == null &&
                                  deviatie.Module == null))
                            {
                                tempnew.Add(deviatie);
                            }
                            break;
                    }
                }

                return tempnew;
            }
        }


        #region Writing
        public void Write(string file, ObservableCollection<Deviatie> deviatiesObservableCollection)
        {
            // write with a retry build in ... 
            var retryamount = Retry;
            var retrytime = WaitTime;


            while (true)
            {
                try
                {
                    WriteXML(file, deviatiesObservableCollection);
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }
        }
        public async void WriteAsync(string file, ObservableCollection<Deviatie> deviatiesObservableCollection)
        {
            var taak = Task.Factory.StartNew(() => Write(file, deviatiesObservableCollection));

            await taak;
        }
        private void WriteXML(string file, ObservableCollection<Deviatie> deviatiesObservableCollection)
        {
            //create the xml settings.
            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = true
            };


            //write the xml
            using (var filestream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            using (var writer = XmlWriter.Create(filestream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Deviaties");


                foreach (var dev in deviatiesObservableCollection)
                {
                    writer.WriteStartElement("Deviatie");

                    writer.WriteElementString("Project", dev.Project);
                    writer.WriteElementString("Module", dev.Module);
                    writer.WriteElementString("Faultcode", dev.FaultCode);
                    writer.WriteElementString("Comment", dev.Comment);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    #endregion
    }
}