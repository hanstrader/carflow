﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Carflow
{
    interface IWriterReader
    {
        Task<System.Collections.ObjectModel.ObservableCollection<Deviatie>> ReadAsync(string file);
        System.Collections.ObjectModel.ObservableCollection<Deviatie> Read(string file);

        void WriteAsync(string file, ObservableCollection<Deviatie> deviatiesObservableCollection);
        void Write(string file, ObservableCollection<Deviatie> deviatiesObservableCollection);
    }
}
