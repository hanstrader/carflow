﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using CarFlow.CarData;
using CarFlow.Deviations;
using CarFlow.MatrixBuilder;

namespace CarFlow.Xlist
{
    public class XList
    {
        const int HeaderLenght = 5;
        private const string Symbol = "x";


        /// <summary>
        /// this method creates a xlist based on all runs.
        /// </summary>
        /// <param name="cars">the cars</param>
        /// <returns></returns>
        public static List<string[]> AllRuns(List<Car> cars)
        {
            var result = new List<string[]>();
            int aantalwagens = cars.Count;

            for (int i = 0; i < cars.Count; i++)
            {
                foreach (var fout in cars[i].Matrix)
                {
                    //skip the header
                    if (fout[0] == "Process") continue;

                    var index =
                        result.FindIndex(o => o[0] == fout[0] && o[1] == fout[1] && o[2] == fout[2] && o[3] == fout[3]);
                    
                    if (index < 0) //not found
                    {
                        string[] lijn = new string[aantalwagens+HeaderLenght];
                        lijn[0] = fout[0]; //process
                        lijn[1] = fout[1]; //test
                        lijn[2] = fout[2]; //faultcode
                        lijn[3] = fout[3]; //faultdescription
                        lijn[4] = (Convert.ToInt32(lijn[4]) + 1).ToString();

                        lijn[i + HeaderLenght] = Symbol;

                        //group it to the same test if possible
                        var insertplace = result.FindLastIndex(o => o[0] == fout[0] && o[1] == fout[1]);
                        if (insertplace >= 0)
                        {
                            result.Insert(insertplace + 1, lijn);
                        }
                        else
                        {
                            //group it to the same process if possible
                            insertplace = result.FindLastIndex(o => o[0] == fout[0]);
                            if (insertplace > 0)
                            {
                                result.Insert(insertplace + 1, lijn);
                            }
                            else
                            {
                                result.Add(lijn);
                            }
                        }
                    }
                    else //found
                    {
                        result[index][i + HeaderLenght] = Symbol;
                        result[index][4] = (Convert.ToInt32(result[index][4]) + 1).ToString();
                    }
                }
            }

            //add the header
            result.Insert(0,CreateHeader(cars));

            return result;
        }
        public static List<string[]> AllRuns(List<Car> cars, List<Deviatie> deviaties )
        {
            var result = new List<string[]>();
            int aantalwagens = cars.Count;

            for (int i = 0; i < cars.Count; i++)
            {
                foreach (var fout in cars[i].Matrix)
                {
                    //skip the header
                    if (fout[0] == "Process") continue;

                    var index = result.FindIndex(o => o[0] == fout[0] && o[1] == fout[1] && o[2] == fout[2] && o[3] == fout[3]);

                    if (index < 0) //not found
                    {
                        string[] lijn = new string[aantalwagens + HeaderLenght+1];
                        lijn[0] = fout[0]; //process
                        lijn[1] = fout[1]; //test
                        lijn[2] = fout[2]; //faultcode
                        lijn[3] = fout[3]; //faultdescription
                        lijn[4] = (Convert.ToInt32(lijn[4]) + 1).ToString();

                        lijn[i + HeaderLenght] = Symbol;

                        //add the devatie text
                        if (deviaties != null)
                        {
                            var gevondenDeviatie = deviaties.Find(o => o.FaultCode == fout[2] && fout[1].Contains(o.Module));
                            if (gevondenDeviatie != null)
                            {
                                lijn[cars.Count + HeaderLenght] = gevondenDeviatie.Comment;
                            }
                        }

                        //group it to the same test if possible
                        var insertplace = result.FindLastIndex(o => o[0] == fout[0] && o[1] == fout[1]);
                        if (insertplace >= 0)
                        {
                            result.Insert(insertplace + 1, lijn);
                        }
                        else
                        {
                            //group it to the same process if possible
                            insertplace = result.FindLastIndex(o => o[0] == fout[0]);
                            if (insertplace > 0)
                            {
                                result.Insert(insertplace + 1, lijn);
                            }
                            else
                            {
                                result.Add(lijn);
                            }
                        }
                  

                    }
                    else //found
                    {
                        result[index][i + HeaderLenght] = Symbol;
                        result[index][4] = (Convert.ToInt32(result[index][4]) + 1).ToString();
                    }
                }
            }

            //add the header
            result.Insert(0, CreateHeader(cars));

            return result;
        }

        /// <summary>
        /// this method creates a xlist based on the first runs only
        /// </summary>
        /// <param name="cars"></param>
        /// <returns></returns>
        public static List<string[]> FirstRuns(List<Car> cars)
        {
            var result = new List<string[]>();
            int aantalwagens = cars.Count;

            for (int i = 0; i < cars.Count; i++)
            {
                var filteredMatrix = FilterForFirstRun(cars[i]);

                foreach (var fout in filteredMatrix)
                {
                    //skip the header
                    if (fout[0] == "Process") continue;

                    var index =
                        result.FindIndex(o => o[0] == fout[0] && o[1] == fout[1] && o[2] == fout[2] && o[3] == fout[3]);

                    if (index < 0) //not found
                    {
                        string[] lijn = new string[aantalwagens + HeaderLenght];
                        lijn[0] = fout[0]; //process
                        lijn[1] = fout[1]; //test
                        lijn[2] = fout[2]; //faultcode
                        lijn[3] = fout[3]; //faultdescription
                        lijn[4] = (Convert.ToInt32(lijn[4]) + 1).ToString();

                        lijn[i + HeaderLenght] = Symbol;

                        //group it to the same test if possible
                        var insertplace = result.FindLastIndex(o => o[0] == fout[0] && o[1] == fout[1]);
                        if (insertplace >= 0)
                        {
                            result.Insert(insertplace + 1, lijn);
                        }
                        else
                        {
                            //group it to the same process if possible
                            insertplace = result.FindLastIndex(o => o[0] == fout[0]);
                            if (insertplace > 0)
                            {
                                result.Insert(insertplace + 1, lijn);
                            }
                            else
                            {
                                result.Add(lijn);
                            }
                        }
                    }
                    else //found
                    {
                        result[index][i + HeaderLenght] = Symbol;
                        result[index][4] = (Convert.ToInt32(result[index][4]) + 1).ToString();
                    }
                }
            }

            //add the header
            result.Insert(0, CreateHeader(cars));

            return result;

        }
        public static List<string[]> FirstRuns(List<Car> cars, List<Deviatie> deviaties)
        {
            var result = new List<string[]>();
            int aantalwagens = cars.Count;

            for (int i = 0; i < cars.Count; i++)
            {
                var filteredMatrix = FilterForFirstRun(cars[i]);

                foreach (var fout in filteredMatrix)
                {
                    //skip the header
                    if (fout[0] == "Process") continue;

                    var index =
                        result.FindIndex(o => o[0] == fout[0] && o[1] == fout[1] && o[2] == fout[2] && o[3] == fout[3]);

                    if (index < 0) //not found
                    {
                        string[] lijn = new string[aantalwagens + HeaderLenght + 1];
                        lijn[0] = fout[0]; //process
                        lijn[1] = fout[1]; //test
                        lijn[2] = fout[2]; //faultcode
                        lijn[3] = fout[3]; //faultdescription
                        lijn[4] = (Convert.ToInt32(lijn[4]) + 1).ToString();

                        lijn[i + HeaderLenght] = Symbol;

                        //add the devatie text
                        if (deviaties != null)
                        {
                            var gevondenDeviatie = deviaties.Find(o => o.FaultCode == fout[2] && fout[1].Contains(o.Module));
                            if (gevondenDeviatie != null)
                            {
                                lijn[cars.Count + HeaderLenght] = gevondenDeviatie.Comment;
                            }
                        }

                        //group it to the same test if possible
                        var insertplace = result.FindLastIndex(o => o[0] == fout[0] && o[1] == fout[1]);
                        if (insertplace >= 0)
                        {
                            result.Insert(insertplace + 1, lijn);
                        }
                        else
                        {
                            //group it to the same process if possible
                            insertplace = result.FindLastIndex(o => o[0] == fout[0]);
                            if (insertplace > 0)
                            {
                                result.Insert(insertplace + 1, lijn);
                            }
                            else
                            {
                                result.Add(lijn);
                            }
                        }
                    }
                    else //found
                    {
                        result[index][i + HeaderLenght] = Symbol;
                        result[index][4] = (Convert.ToInt32(result[index][4]) + 1).ToString();
                    }
                }
            }

            //add the header
            result.Insert(0, CreateHeader(cars));

            return result;

        }


        /// <summary>
        /// this method creates a xlist based on the remaining open items.
        /// </summary>
        /// <param name="cars"></param>
        /// <returns></returns>
        public static List<string[]> OpenIssues(List<Car> cars)
        {
            var result = new List<string[]>();
            int aantalwagens = cars.Count;

            for (int i = 0; i < cars.Count; i++)
            {
                if (cars[i].RemainingIssueMatrix == null) continue;

                foreach (var fout in cars[i].RemainingIssueMatrix)
                {
                    //skip the header
                    if (fout[0] == "Process") continue;

                    var index =
                        result.FindIndex(o => o[0] == fout[0] && o[1] == fout[1] && o[2] == fout[2] && o[3] == fout[3]);

                    if (index < 0) //not found
                    {
                        string[] lijn = new string[aantalwagens + HeaderLenght];
                        lijn[0] = fout[0]; //process
                        lijn[1] = fout[1]; //test
                        lijn[2] = fout[2]; //faultcode
                        lijn[3] = fout[3]; //faultdescription
                        lijn[4] = (Convert.ToInt32(lijn[4]) + 1).ToString();

                        lijn[i + HeaderLenght] = Symbol;

                        //group it to the same test if possible
                        var insertplace = result.FindLastIndex(o => o[0] == fout[0] && o[1] == fout[1]);
                        if (insertplace >= 0)
                        {
                            result.Insert(insertplace + 1, lijn);
                        }
                        else
                        {
                            //group it to the same process if possible
                            insertplace = result.FindLastIndex(o => o[0] == fout[0]);
                            if (insertplace > 0)
                            {
                                result.Insert(insertplace + 1, lijn);
                            }
                            else
                            {
                                result.Add(lijn);
                            }
                        }
                    }
                    else //found
                    {
                        result[index][i + HeaderLenght] = Symbol;
                        result[index][4] = (Convert.ToInt32(result[index][4]) + 1).ToString();
                    }
                }
            }

            //add the header
            result.Insert(0, CreateHeader(cars));

            return result;
        }

        public static List<string[]> OpenIssues(List<Car> cars, List<Deviatie> deviaties)
        {
            var result = new List<string[]>();
            int aantalwagens = cars.Count;

            for (int i = 0; i < cars.Count; i++)
            {
                if (cars[i].RemainingIssueMatrix == null) continue;

                foreach (var fout in cars[i].RemainingIssueMatrix)
                {
                    //skip the header
                    if (fout[0] == "Process") continue;

                    var index =
                        result.FindIndex(o => o[0] == fout[0] && o[1] == fout[1] && o[2] == fout[2] && o[3] == fout[3]);

                    if (index < 0) //not found
                    {
                        string[] lijn = new string[aantalwagens + HeaderLenght+1];
                        lijn[0] = fout[0]; //process
                        lijn[1] = fout[1]; //test
                        lijn[2] = fout[2]; //faultcode
                        lijn[3] = fout[3]; //faultdescription
                        lijn[4] = (Convert.ToInt32(lijn[4]) + 1).ToString();

                        lijn[i + HeaderLenght] = Symbol;

                        //add the devatie text
                        if (deviaties != null)
                        {
                            var gevondenDeviatie = deviaties.Find(o => o.FaultCode == fout[2] && fout[1].Contains(o.Module));
                            if (gevondenDeviatie != null)
                            {
                                lijn[cars.Count + HeaderLenght] = gevondenDeviatie.Comment;
                            }
                        }

                        //group it to the same test if possible
                        var insertplace = result.FindLastIndex(o => o[0] == fout[0] && o[1] == fout[1]);
                        if (insertplace >= 0)
                        {
                            result.Insert(insertplace + 1, lijn);
                        }
                        else
                        {
                            //group it to the same process if possible
                            insertplace = result.FindLastIndex(o => o[0] == fout[0]);
                            if (insertplace > 0)
                            {
                                result.Insert(insertplace + 1, lijn);
                            }
                            else
                            {
                                result.Add(lijn);
                            }
                        }
                    }
                    else //found
                    {
                        result[index][i + HeaderLenght] = Symbol;
                        result[index][4] = (Convert.ToInt32(result[index][4]) + 1).ToString();
                    }
                }
            }

            //add the header
            result.Insert(0, CreateHeader(cars));

            return result;
        }





        /// <summary>
        /// this method creates a header for the xlist.
        /// </summary>
        /// <param name="cars">the cars</param>
        /// <returns></returns>
        private static string[] CreateHeader(List<Car> cars)
        {
            string[] header = new string[cars.Count + HeaderLenght+1];
            header[0] = "Process";
            header[1] = "Test";
            header[2] = "Faultcode";
            header[3] = "Description";
            header[4] = "Count";


            for (int i = 0; i < cars.Count; i++)
            {
                header[i + HeaderLenght] = cars[i].Mixnr;
            }

            //deviatieheader part
            header[cars.Count + HeaderLenght] = "Comment";

            return header;
        }
        
        /// <summary>
        /// this method is a helper function to filter for firstrun
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        private static List<string[]> FilterForFirstRun(Car car)
        {
            var matrix = car.Matrix;
            var result = new List<string[]>();
            var evaluatedProcesses = new List<string>();

            for (int i = 0; i < car.ProcessHistory.Count; i++)
            {
                if (evaluatedProcesses.Contains(car.ProcessHistory[i].Process)) 
                    continue;
               
                evaluatedProcesses.Add(car.ProcessHistory[i].Process);

                foreach (var line in matrix)
                {
                    //skip the header
                    if (line[0] == "Process") continue;

                    if (line[i + MatrixBase.HeaderLength] != null)
                    {
                        var fout = new string[] {line[0], line[1], line[2], line[3]};
                        result.Add(fout);
                    }
                }
            }

            return result;
        }
        
    }
}
