﻿/* This class holds the data of one car
 * This 'car' is having several properties that represent the raw QBAY data.
 * Process history, testdetails en faultcodes from the cars are placed here.
 * 
 * This also holds the Mainmatrix and the MainTable version but these are created by the Mainworker class
 * this also holds the remainingissueMatrix and the remainingissuetable version.
 * 
 * If extra data like buildcode, readouts and etc should be collected, then this should be stored in the class as raw data.
 *  
 *  */


using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using SOLIDQBAY;
using System.Data.Linq;
using System.IO;
using System.Linq;
using CarFlow.Helper;

namespace CarFlow.CarData
{
    /// <summary>
    /// Base Class for CarData
    /// </summary>
    public abstract class CarData: BaseNotify
    {
        private List<QbayProcessHistoryLine> _processHistory;
        private Dictionary<string, QbayTestDetails> _testDetails = new Dictionary<string, QbayTestDetails>();
        private List<QbayFaultCodesByVinLine> _faultCodes;

        /// <summary>
        /// this value holds the total number of NOK fields. This can be used for loadbalancing if we use multithreading for loading the data.
        /// </summary>
        internal int TotalNoKfields { get; set; }

        /// <summary>
        /// Mixnr of the car
        /// </summary>
        public string Mixnr { get; set; }

        /// <summary>
        /// the processhistory lines for this mixnr. the processes will be ordererd.
        /// </summary>
        internal List<QbayProcessHistoryLine> ProcessHistory
        {
            get { return _processHistory; }
            set
            {
                _processHistory = value;
                _processHistory = _processHistory.OrderBy(o => o.Starttime).ToList();
            }
        }

        /// <summary>
        /// the testdetails for the mixnr
        /// </summary>
        internal Dictionary<string, QbayTestDetails> TestDetails
        {
            get { return _testDetails; }
            set
            {
                _testDetails = value;

                TotalNoKfields = GetTotalNokFields();
            }
        }

        /// <summary>
        /// All the faultcodes of this car. The list will be ordered.
        /// </summary>
        internal List<QbayFaultCodesByVinLine> FaultCodes
        {
            get { return _faultCodes; }
            set
            {
                _faultCodes = value;
                _faultCodes = FaultCodes.OrderBy(o => o.TestTime).ToList();
            }
        }

        /// <summary>
        /// All the numericparameters of this car. 
        /// </summary>
        public List<QbayNumericParametersLine> NumericParameters { get; set; }

        /// <summary>
        /// All the textparameter of this car.
        /// </summary>
        public List<QbayTextParametersLine> TextParameters { get; set; }

        /// <summary>
        /// All the buldcodes of this car.
        /// </summary>
        public QbayBuildCode EPlabel { get; set; }

        /// <summary>
        /// calculate  the totalNOKfields in the complete testdetailsList. This can be used for load balancing.
        /// </summary>
        /// <returns></returns>
        internal int GetTotalNokFields()
        {
            int amountNok = 0;

            foreach (var detailpage in TestDetails.Values)
            {
                foreach (var test in detailpage.Lines)
                {
                    if (test.Status != "OK") amountNok++;
                }
            }
            return amountNok;
        }

        /// <summary>
        /// constructor
        /// </summary>
        protected CarData()
        {

        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mixnr">Mixnr of the car</param>
        protected CarData(string mixnr)
        {
            Mixnr = mixnr;
        }

     }


    public class Car : CarData
    {
        private List<string[]> _matrix;
        private List<string[]> _remainingIssueMatrix;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mixnr">Mixnr of the car</param>
        public Car(string mixnr)
        {
            Mixnr = mixnr;
        }

        /// <summary>
        /// default constructor
        /// </summary>
        public Car()
        {
            
        }

        /// <summary>
        /// Copies the Hrefline from the processhistory to the testdetails.
        /// </summary>
        internal void CopyHistoryHrefToDetails()
        {
            foreach (var historyline in ProcessHistory)
            {
                TestDetails.Add(historyline.ProcessHref,null);
            }
        }


        /// <summary>
        /// the matrix that is displayed by carflow.
        /// </summary>
        public List<string[]> Matrix
        {
            get { return _matrix; }
            set
            {
                _matrix = value;
                MatrixDataTable = MatrixToTable(value);
            }
        }

        /// <summary>
        /// the matrix that is displayed by carflow.
        /// </summary>
        public List<string[]> RemainingIssueMatrix
        {
            get { return _remainingIssueMatrix; }
            set
            {
                _remainingIssueMatrix = value;
                RemainIssueDataTable = MatrixToTable(value);
            }
        }

        /// <summary>
        /// The datatable representing the MainMatrix
        /// </summary>
        public DataTable MatrixDataTable { get; set; }

        /// <summary>
        /// The datatable representing the remaining issues.
        /// </summary>
        public DataTable RemainIssueDataTable { get; set; }


        /// <summary>
        /// method that creates a table based on a matrix.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private DataTable MatrixToTable(List<string[]> source)
        {
            if (source == null) return null;

            DataTable table = new DataTable();

            //add columns
            for (int index = 0; index < source[0].Length; index++)
            {
                DataColumn column = table.Columns.Add();
                column.MaxLength = 999;
                try
                {
                    column.ColumnName = source[0][index].Replace("(", "t = ").Replace(")", " sec").Replace("/", "-");
                }
                catch
                {
                    column.ColumnName = source[0][index].Replace("(", "t = ").Replace(")", " sec").Replace("/", "-")+"\r\n duplicate";
                }
            }

            if (source.Count == 1) return table;

            //add the rows
            for (int i = 1; i < source.Count; i++)
            {
                var rij = table.Rows.Add(source[i]);
            }

            

            return table;
        }


    }

}
