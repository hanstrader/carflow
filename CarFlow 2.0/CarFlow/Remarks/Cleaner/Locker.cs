﻿using System.IO;

namespace CarFlow.Remarks.Cleaner
{
    internal class Locker : ILocker
    {
        private bool _locked;
        private const string filename = "LOCK.txt";

        public string DirectoryToLock { get; set; }

        public bool Locked
        {
            get { return _locked; }
        }

        public void Lock()
        {
            if (Directory.Exists(DirectoryToLock) )
            {
                if (File.Exists(Path.Combine(DirectoryToLock, filename)))
                {
                    return;
                }

                using (var file = File.CreateText(Path.Combine(DirectoryToLock, filename)))
                {
                    file.WriteLine("this is the lockfile to clean up the xml's");
                    file.Flush();
                    file.Close();

                    _locked = true;
                }
            }
        }

        public void Unlock()
        {
            try
            {
                if (Directory.Exists(DirectoryToLock) && Locked)
                {
                    File.Delete(Path.Combine(DirectoryToLock, filename));

                    _locked = false;
                }
            }
            catch
            {
                //ignored.
            }

        }
    }
}
