﻿namespace CarFlow.Remarks.Cleaner
{
    internal interface ILocker
    {
        string DirectoryToLock { get; set; }
        bool Locked { get; }
        void Lock();
        void Unlock();
    }
}