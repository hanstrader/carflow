﻿

namespace CarFlow.Deviations
{
    public class Deviatie
    {
        public string Project { get; set; }

        public string Serie { get; set; }

        public string Module { get; set; }

        public string FaultCode { get; set; }

        public string Comment { get; set; }
    }



}
