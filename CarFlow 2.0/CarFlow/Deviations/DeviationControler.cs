﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using CarFlow.Helper;

namespace CarFlow.Deviations
{
    public class DeviationControler : BaseNotify
    {
        internal const string FileName = "CarFlowDeviaties.xml";

        /// <summary>
        /// the constructor
        /// </summary>
        public DeviationControler()
        {
            //inject the readerwriter
            _writerReader = new DeviationXmlWriterReader();

            //set up the filesystemwatcher
            _watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime;
            _watcher.Filter = FileName;
            _watcher.Changed += WatcherChanged;
            _watcher.Error += WatcherError;
        }

        
        /// <summary>
        /// the drive where the deviations will be located.
        /// </summary>
        public string FileLocation
        {
            get { return _fileLocation; }
            set
            {
                if (Directory.Exists(value))
                {
                    _fileLocation = value;

                    _watcher.Path = value;
                    _watcher.EnableRaisingEvents = true;

                    OnPropertyChanged();


                    if (!File.Exists(Path.Combine(value, FileName)))
                    {
                        Deviaties = null;
                        return;
                    }

                    Deviaties = _writerReader.Read(Path.Combine(value, FileName));

                }
            }
        }

        /// <summary>
        /// the deviation list
        /// </summary>
        public ObservableCollection<Deviatie> Deviaties
        {
            get
            {
                if(_deviaties == null)
                {  _deviaties = new ObservableCollection<Deviatie>();}
                return _deviaties;
            }
            set
            {
                _deviaties = value; 

                ProjectsList = GetPossibleProject(Deviaties);
                SeriesList = GetPossibleSeries(Deviaties);
                UpdateActiveDeviations();

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// method updates the deviation on the drive.
        /// </summary>
        public void UpdateDeviaties()
        {

            if (FileLocation == null) return;
             
            _writerReader.Write(Path.Combine(FileLocation, FileName), Deviaties);

            ProjectsList = GetPossibleProject(Deviaties);
            SeriesList = GetPossibleSeries(Deviaties);

        }

        /// <summary>
        /// Get's the possible projects in the deviations
        /// </summary>
        public List<string> ProjectsList
        {
            get { return _projectsList; }
            private set
            {
                _projectsList = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// the Project that is selected for the deviations
        /// </summary>
        public string SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                UpdateActiveDeviations();

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// the serie that is seleceted for the deviations
        /// </summary>
        public string SelectedSerie
        {
            get { return _selectedSerie; }
            set
            {
                _selectedSerie = value;
                UpdateActiveDeviations();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// the list of active deviations
        /// </summary>
        public List<Deviatie> ActiveDeviaties
        {
            get { return _activeDeviaties; }
            set
            {
                _activeDeviaties = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Get's the possible Series in the deviations.
        /// </summary>
        public List<string> SeriesList
        {
            get { return _seriesList; }
            set
            {
                _seriesList = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Clear's all the deviation
        /// </summary>
        public void Clear()
        {
            if (Deviaties != null) Deviaties.Clear();
        }

        /// <summary>
        /// Add's a deviation to the list.
        /// </summary>
        /// <param name="dev"></param>
        public void Add(Deviatie dev)
        {
            if (Deviaties.Count == 0)
            {
                Deviaties.Insert(0, dev);
                UpdateDeviaties();
                return;
            }

            try
            {
                if (Deviaties.Contains(Deviaties.Single(i => i.Comment == dev.Comment &&
                                                              i.FaultCode == dev.FaultCode &&
                                                              i.Module == dev.Module &&
                                                              i.Project == dev.Project &&
                                                              i.Serie == dev.Serie)))
                {
                    return;
                }

            }
            catch
            {
                Deviaties.Insert(0, dev);
                UpdateDeviaties();
            }
        }


        /// <summary>
        /// Delete's a deviation from the list.
        /// </summary>
        /// <param name="dev"></param>
        public void Delete(Deviatie dev)
        {
            try
            {
                Deviaties.Remove(Deviaties.Single(i => i.Comment == dev.Comment &&
                                                       i.FaultCode == dev.FaultCode &&
                                                       i.Module == dev.Module &&
                                                       i.Project == dev.Project &&
                                                       i.Serie == dev.Serie));
            }
            catch
            {
                return;
            }

            UpdateDeviaties();
        }



        private void WatcherChanged(object sender, FileSystemEventArgs e)
        {
            Deviaties = _writerReader.ReadAsync(Path.Combine(FileLocation, FileName)).Result;
        }
        private void WatcherError(object sender, ErrorEventArgs e)
        {
            _watcher.EnableRaisingEvents = false;
        }

        private void UpdateActiveDeviations()
        {
            if (SelectedSerie == null && SelectedProject == null) return;

            ActiveDeviaties = _deviaties.Where(x => x.Serie == SelectedSerie && x.Project == SelectedProject).ToList();
        }


        private List<string> GetPossibleProject(ObservableCollection<Deviatie> dev )
        {
            List<string> result = new List<string>();

            foreach (var line in dev)
            {
                if(!result.Contains(line.Project)) result.Add(line.Project);
            }

            return result;
        }
        private List<string> GetPossibleSeries(ObservableCollection<Deviatie> dev)
        {
            List<string> result = new List<string>();

            foreach (var line in dev)
            {
                if (!result.Contains(line.Serie)) result.Add(line.Serie);
            }

            return result;
        }
        private readonly FileSystemWatcher _watcher = new FileSystemWatcher();
        private readonly IWriterReader _writerReader;
        private string _fileLocation;
        private ObservableCollection<Deviatie> _deviaties = new ObservableCollection<Deviatie>();
        private List<string> _projectsList;
        private List<string> _seriesList;
        private string _selectedProject;
        private string _selectedSerie;
        private List<Deviatie> _activeDeviaties;
    }
}