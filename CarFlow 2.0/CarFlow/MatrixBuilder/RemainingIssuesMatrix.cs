using System.Collections.Generic;
using CarFlow.CarData;

namespace CarFlow.MatrixBuilder
{
    internal class RemainingIssuesMatrix : MatrixBase
    {
        /// <summary>
        /// static method that builds a remaining issue matrix in the car provided.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        internal static List<string[]> Create(List<string[]> matrix)
        {
            var mainMatrix = matrix;

            var resultMatrix = new List<string[]> {matrix[0]};

            //add the header
            if (matrix.Count == 1)
            {
                return resultMatrix;
            }

            //loop door elke regel van boven tot onder
            for (int regel = 1; regel < mainMatrix.Count; regel++)
            {
                string[] t = mainMatrix[regel];
                
                //loop in elke regel van rechts naar links;
                for (var kolom = t.Length - 1; kolom > HeaderLength-1; kolom--)
                {
                    if (t[kolom] == null) continue;
                    if (t[kolom] != "OK")
                    {
                        resultMatrix.Add(t);
                    }
                    break;
                }
            }

            return resultMatrix;
        }

        /// <summary>
        /// static method that builds a remaining issue matrix based on the matrix provided.
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        internal static List<string[]> Create(Car car)
        {
            if (car == null || car.Matrix == null)
            {
                return null;
                
            }

            var remainMatrix = Create(car.Matrix);

            return remainMatrix;
        }
    }
}