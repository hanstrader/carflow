﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Configuration.Assemblies;
using CarFlow.Properties;

namespace CarFlow.SettingsSaver
{
    public class SettingsSave
    {
        public SettingsSave()
        {
            SetPlant = Settings.Default.SetSelectedPlant;
            SetDeviationDirectory = Settings.Default.SetDirectoryForDeviations;
        }

        public string SetPlant { get; set; }
        public string SetDeviationDirectory { get; set; }

        public void Save()
        {
            Settings.Default.SetDirectoryForDeviations = SetDeviationDirectory;
            Settings.Default.SetSelectedPlant = SetPlant;
            Settings.Default.Save();
        }


    }
}
