﻿using System;
using System.IO;
using CarFlow.Deviations;
using Microsoft.VisualStudio.TestTools.UnitTesting;



namespace DeviationTests
{
    [TestClass()]
    public class DeviationControlerTests
    {

        [TestMethod()]
        public void DeviationControlerTest()
        {
            var control = new DeviationControler();
            var dev1 = new Deviatie {Comment = "eerste", FaultCode = "AAAAAA", Module = "CEM", Project = "V451", Serie = "VP1.3"};
            var dev2 = new Deviatie {Comment = "tweede", FaultCode = "BBBBBB", Module = "PAM", Project = "Y555", Serie = "VP2.3" };

            //test met lege control
            control.UpdateDeviaties();

            //test met niet bestaande directory
            control.FileLocation = "blablabla";
            Assert.IsTrue(control.FileLocation == null);

            //test met bestaande directory
            control.FileLocation = AppDomain.CurrentDomain.BaseDirectory;
            Assert.IsFalse(control.FileLocation == null);


            //test met lege deviatie file
            try
            {
                File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CarFlowDeviaties.xml"));
            }
            catch
            {
                // ignored
            }     

            //test met deviaties
            control.Clear();
            control.Add(dev1);
            control.Add(dev2);
            Assert.IsTrue(control.Deviaties.Count==2);

            ////stresstest met 2000 deviaties en veelvuldig async schrijven.
            for (int i = 0; i < 2000; i++)
            {
                control.Deviaties.Add(dev1);
            }
            control.UpdateDeviaties();
        }
    }
}
