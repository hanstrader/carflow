﻿using System.Collections.Generic;
using SOLIDQBAY;

namespace CarData
{
    public abstract class CarData
    {
        private string _mixnr;

        /// <summary>
        /// Mixnr of the car
        /// </summary>
        public string Mixnr
        {
            get { return _mixnr; }
            set { _mixnr = value; }
        }

        /// <summary>
        /// the processhistory lines for this mixnr.
        /// </summary>
        public List<QbayProcessHistoryLine> ProcessHistoryLines { get; private set; }

        /// <summary>
        /// the testdetails for the mixnr
        /// </summary>
        public Dictionary<string, QbayTestDetails> TestDetailsList { get; private set; }

        /// <summary>
        /// constructor
        /// </summary>
        public CarData()
        {

        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mixnr">Mixnr of the car</param>
        public CarData(string mixnr)
        {
            _mixnr = mixnr;
        }

     }


    public class Car : CarData
    {
        
    }

}
