﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Remark;


namespace RemarkCleanUp
{
    public class Cleaner
    {
        public Cleaner(RemarkControler controler)
        {
            _timer.Elapsed += timer_Elapsed;
            _controler = controler;
            _lock = new Locker();
           
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            Clean();
            _timer.Start();
        }

        private ILocker _lock;
        private RemarkControler _controler;


        private void Clean()
        {
            _lock.Lock();

            if (!_lock.Locked) return;







            _lock.Unlock();
        }

        private readonly Timer _timer = new Timer();

        public int TimeOut
        {
            set { _timer.Interval = value; }
        }

        public bool Enable
        {
            set { _timer.Enabled = value; }
        }



    }
}
