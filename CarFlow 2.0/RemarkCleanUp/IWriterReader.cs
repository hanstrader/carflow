﻿namespace RemarkCleanUp
{
    interface IWriterReader
    {
        void Read();
        void ReadAsync();

        void Write();
        void WriteAsync();
    }
}
