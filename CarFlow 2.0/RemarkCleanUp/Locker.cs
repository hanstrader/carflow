﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Runtime;

namespace RemarkCleanUp
{
    internal class Locker : ILocker
    {
        private bool _locked;
        private string _directoryToLock;
        private const string filename = "LOCK.txt";

        public string DirectoryToLock
        {
            get { return _directoryToLock; }
            set { _directoryToLock = value; }
        }

        public bool Locked
        {
            get { return _locked; }
        }

        public void Lock()
        {
            if (Directory.Exists(DirectoryToLock) )
            {
                if (File.Exists(Path.Combine(DirectoryToLock, filename)))
                {
                    return;
                }

                var file = File.CreateText(Path.Combine(DirectoryToLock, filename));
                file.WriteLine("this is the lockfile to clean up the xml's");
                file.Flush();
                file.Close();
                
                _locked = true;
            }
        }

        public void Unlock()
        {
            if (Directory.Exists(DirectoryToLock) && Locked)
            {
                File.Delete(Path.Combine(DirectoryToLock, filename));

                _locked = false;
            }
            
        }
    }
}
