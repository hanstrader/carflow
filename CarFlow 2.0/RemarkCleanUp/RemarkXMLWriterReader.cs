﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace RemarkCleanUp
{
    internal class RemarkXmlWriterReader : IWriterReader
    {
        internal RemarkXmlWriterReader(RemarkControler controler)
        {
            _controler = controler;
        }

        private readonly RemarkControler _controler;


        private string File
        {
            get
            {
                return Path.Combine(_controler.FileLocation, RemarkControler.FileName);
            }

        }


        private Remark _remark;

        private void Write()
        {
            RetryMethod(WriteXml, 10, 100);
        }


        public async void WriteAsync()
        {
            var taak = new Task(Write);

            taak.Start();
            await taak;
        }


        public void Write(Remark remark)
        {
            _remark = remark;
            RetryMethod(WriteXml, 10, 100);
        }


        public async void WriteAsync(Remark remark)
        {
            _remark = remark;

            var taak = new Task(Write);
            taak.Start();
            await taak;
        }


        public void Read()
        {
            RetryMethod(ReadXML, 10, 1000);
        }


        public async void ReadAsync()
        {
            var taak = new Task(Read);

            taak.Start();
            await taak;
        }


        private void WriteXml()
        {
            var filename = GetFileNameOutOfRemark();

            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = true
            };


            using (var filestream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            using (var writer = XmlWriter.Create(filestream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Remark");

                writer.WriteElementString("Project", _remark.Project);
                writer.WriteElementString("process", _remark.Process);
                writer.WriteElementString("Test", _remark.Test);
                writer.WriteElementString("FaultCode", _remark.Fault);
                writer.WriteElementString("Description", _remark.Description);
                writer.WriteElementString("Comment", _remark.Comment);
                writer.WriteElementString("VQDC", _remark.VQDC);
                writer.WriteElementString("SBS", _remark.SBS);

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }


        private void ReadXML()
        {
            if (!System.IO.File.Exists((File)))
            {
                return;
            }

            var tempnew = new ObservableCollection<Remark>();
            var remark = new Remark();


            using (var filestream = new FileStream(File, FileMode.Open, FileAccess.Read, FileShare.None))
            using (var reader = XmlReader.Create(filestream))
            {
                while (reader.Read())
                {
                    if (!reader.IsStartElement()) continue;

                    switch (reader.Name)
                    {
                        case "Remarks":
                            if (!(remark.Project == null && remark.Process == null && remark.Test == null && remark.Fault == null))
                            {
                                tempnew.Add(remark);
                            }
                            remark = new Remark();
                            break;

                        case "Project":
                            remark.Project = reader.Read() ? reader.Value : "";
                            break;

                        case "Process":
                            remark.Process = reader.Read() ? reader.Value : "";
                            break;

                        case "Test":
                            remark.Test = reader.Read() ? reader.Value : "";
                            break;

                        case "Fault":
                            remark.Fault = reader.Read() ? reader.Value : "";
                            break;

                        case "Description":
                            remark.Description = reader.Read() ? reader.Value : "";
                            break;

                        case "Comment":
                            remark.Comment = reader.Read() ? reader.Value : "";
                            break;

                        case "VQDC":
                            remark.VQDC = reader.Read() ? reader.Value : "";
                            break;

                        case "SBS":
                            remark.SBS = reader.Read() ? reader.Value : "";
                            break;
                            
                    }
                }

                _controler.RemaksObservableCollection = tempnew;
            }



        }


        private void RetryMethod(Action actie, int amount, int intervaltime)
        {
            // write with a retry build in ... 
            var retryamount = amount;
            var retrytime = intervaltime;

            while (true)
            {
                try
                {
                    actie();
                    break;
                }
                catch
                {
                    retryamount--;

                    if (retryamount != 0) Thread.SpinWait(retrytime);
                    else
                    {
                        break;
                    }
                }
            }

        }

    
    }
        
}
