﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carflow.DataObjects;

namespace Carflow.TableBuilder
{
    public class RemainingIssuesMatrix : MatrixBase
    {
        /// <summary>
        /// static method that builds a remaining issue matrix in the car provided.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static List<string[]> Create(List<string[]> matrix)
        {
            var mainMatrix = matrix;

            var temp = new List<string[]>();

            //add the header
            temp.Add(matrix[0]);

            if (matrix.Count == 1)
            {
                return temp;
            }

            //loop door elke regel van boven tot onder
            for (int index = 1; index < mainMatrix.Count; index++)
            {
                string[] t = mainMatrix[index];

                //loop in elke regel van rechts naar links;
                for (int j = t.Length - 1; j > HeaderLength - 1; j--)
                {
                    if (t[j] != null)
                    {
                        if (t[j] != "OK")
                        {
                            temp.Add(t);
                        }
                        break;
                    }
                }
            }

            return temp;
        }

        /// <summary>
        /// static method that builds a remaining issue matrix based on the matrix provided.
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        public static List<string[]> Create(Car car)
        {
            if (car == null || car.Matrix == null) return null;

            var remainMatrix = Create(car.Matrix);

            return remainMatrix;
        }
    }
}
