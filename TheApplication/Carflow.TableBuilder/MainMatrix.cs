﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carflow.DataObjects;
using SOLIDQBAY;

namespace Carflow.TableBuilder
{
    public class MainMatrix : MatrixBase
    {


        /// <summary>
        /// static method that builds the matrix in the car provided.
        /// </summary>
        /// <param name="car">the car that should get the matrix </param>
        /// <returns></returns>
        public static List<string[]> Create(Car car)
        {
            var Result = new List<string[]>();

            //ga door elke processHistory. Index used = i
            for (int i = 0; i < car.ProcessHistory.Count; i++)
            {
                #region loop door de ok's
                //loop door de OK's
                if (Result.Count != 0)
                {
                    foreach (var test in car.TestDetails.ElementAt(i).Value.Lines)
                    {
                        //if (test.Status != "OK") continue;
                        for (int j = 0; j < Result.Count; j++)
                        {
                            if (Result[j][0] == car.ProcessHistory[i].Process && Result[j][1] == test.TestDetails)
                            {
                                Result[j][i + HeaderLength] = "OK";
                            }
                        }
                    }
                }
                #endregion

                #region extract faultcodes and cleanup
                //fouten extrageren die overeenkomen met dit process
                List<QbayFaultCodesByVinLine> faults;

                if (i < car.ProcessHistory.Count - 1)
                {
                    faults = ExtractFaults(car.FaultCodes, car.ProcessHistory[i].Starttime, car.ProcessHistory[i + 1].Starttime);
                }
                else
                {
                    faults = ExtractFaults(car.FaultCodes, car.ProcessHistory[i].Starttime, DateTime.MaxValue);
                }


                //remove the masked and fix the correct status
                faults = RemoveMasked(faults);
                Adjuststatus(faults, car.TestDetails.ElementAt(i).Value.Lines);
                #endregion

                #region loop door de nok's
                //als er nog geen items zijn.
                if (Result.Count == 0)
                {
                    foreach (var fault in faults)
                    {
                        string[] line = new string[car.ProcessHistory.Count + HeaderLength];
                        line[0] = fault.Process;
                        line[1] = fault.TestName;
                        line[2] = fault.Faultcodes;
                        line[3] = fault.Faultlabel;
                        line[i + HeaderLength] = fault.Status;

                        Result.Add(line);
                    }
                    continue;
                }

                //ga door de NOK's
                foreach (var fault in faults)
                {
                    int indexprocess = -1;
                    int indextest = -1;
                    bool found = false;

                    for (var j = 0; j < Result.Count; j++)
                    {
                        if (Result[j][0] == car.ProcessHistory[i].Process)
                        {
                            indexprocess = j;

                            if (Result[j][1] == fault.TestName)
                            {
                                indextest = j;

                                if (Result[j][2] == fault.Faultcodes)
                                {
                                    Result[j][i + HeaderLength] = fault.Status;
                                    found = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (found) continue;

                    //if not found.
                    string[] line = new string[car.ProcessHistory.Count + HeaderLength];
                    line[0] = fault.Process;
                    line[1] = fault.TestName;
                    line[2] = fault.Faultcodes;
                    line[3] = fault.Faultlabel;
                    line[i + HeaderLength] = fault.Status;

                    if (indextest == -1)
                    {
                        if (indexprocess == -1)
                        {
                            Result.Add(line);
                        }
                        else
                        {
                            Result.Insert(indexprocess + 1, line);
                        }
                    }
                    else
                    {
                        Result.Insert(indextest + 1, line);
                    }
                }
                #endregion

            }

            //add the header.
            var header = CreateHeader(car);
            Result.Insert(0, header);

            return Result;
        }


        /// <summary>
        /// static method that builds the matrix in the car provided in an Async way.
        /// </summary>
        /// <param name="car">the car that should get the matrix </param>
        /// <returns></returns>
        internal static async Task<List<string[]>> CreateAsync(Car car)
        {
            var taak = Task.Run(() => Create(car));
            await taak;

            return taak.Result;
        }

        /// <summary>
        /// this method extract the faultcodes between 2 timestamps.
        /// </summary>
        /// <param name="lijst">the list that should be filtered</param>
        /// <param name="start">the start timestamp</param>
        /// <param name="end">the end timestamp</param>
        /// <returns></returns>
        private static List<QbayFaultCodesByVinLine> ExtractFaults(List<QbayFaultCodesByVinLine> lijst, DateTime start, DateTime end)
        {
            #region Oldversion
            //int indexstart=0, indexeind = 0;

            ////get startindex
            //for (int i = 0; i < lijst.Count; i++)
            //{
            //    indexstart = i;
            //    if (lijst[i].TestTime >= start) break;
            //}

            //indexeind = indexstart;

            ////get endindex
            //for (int i = indexstart; i < lijst.Count; i++)
            //{
            //    if (lijst[i].TestTime >= end) break;
            //    indexeind = i;
            //}

            //int lengte = indexeind == indexstart ? 0 : indexeind - indexstart + 1;

            //var result = lijst.GetRange(indexstart, lengte); 
            #endregion

            //return result;

            int indexstart = -1, indexeind = -1;

            //get startindex
            for (int i = 0; i < lijst.Count; i++)
            {
                if (start >= lijst[i].TestTime) indexstart = i;
            }

            //get eindindex
            for (int i = 0; i < lijst.Count; i++)
            {
                if (end > lijst[i].TestTime) indexeind = i;
            }

            var rersult = lijst.GetRange(indexstart + 1, indexeind - indexstart);

            return rersult;
        }


        /// <summary>
        /// This method dispose all the masked items.
        /// </summary>
        /// <param name="lijst">the list that should be filtered.</param>
        /// <returns></returns>
        private static List<QbayFaultCodesByVinLine> RemoveMasked(List<QbayFaultCodesByVinLine> lijst)
        {
            var result = new List<QbayFaultCodesByVinLine>();

            foreach (var fault in lijst)
            {
                if (fault.Status.ToUpper() != "MASKED")
                {
                    result.Add(fault);
                }
            }

            return result;
        }


        /// <summary>
        /// this method adjusts the status in foutenlist to the status of the detailslist. The reason is that abort is not reported in foutenlist.
        /// </summary>
        /// <param name="foutenList"></param>
        /// <param name="detailsList"></param>
        private static void Adjuststatus(List<QbayFaultCodesByVinLine> foutenList, List<QbayTestDetailsLine> detailsList)
        {
            for (int i = 0; i < foutenList.Count; i++)
            {
                var index = detailsList.FindIndex(o => o.TestDetails.ToUpper() == foutenList[i].TestName.ToUpper());

                if (foutenList[i].TestName.Contains("Test Interior"))
                {
                    string p = "don't care";
                    p += p;

                }

                if (index >= 0)
                {
                    foutenList[i].Status = detailsList[index].Status;
                }
            }
        }


        /// <summary>
        /// this methods creates a header.
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        private static string[] CreateHeader(Car car)
        {
            string[] _header = new string[HeaderLength + car.ProcessHistory.Count];

            _header[0] = "Process";
            _header[1] = "Test";
            _header[2] = "Fault";
            _header[3] = "Description";



            for (int i = 0; i < car.ProcessHistory.Count; i++)
            {
                //date
                string temp = car.ProcessHistory[i].Starttime.ToShortDateString() + "\r\n";

                //time
                temp += car.ProcessHistory[i].Starttime.ToLongTimeString() + "\r\n";

                //timespan
                if (i > 0)
                {
                    var span = car.ProcessHistory[i].Starttime - car.ProcessHistory[i - 1].Starttime;
                    temp += span.Days + "d " + span.Hours + "h " + span.Minutes + "m" + "\r\n";
                }
                else
                {
                    temp += "\r\n";
                }

                //duration
                temp += "(" + car.ProcessHistory[i].Duration.ToString() + ")" + "\r\n";

                //equipment
                temp += car.ProcessHistory[i].Tester.Replace("VCATS-", "").Replace("VC-", "") + "\r\n";

                //process
                temp += car.ProcessHistory[i].Process;

                //add to the header.
                _header[i + HeaderLength] = temp;

            }


            return _header;

        }

    }
}
