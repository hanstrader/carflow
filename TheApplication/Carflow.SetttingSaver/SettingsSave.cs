﻿using Carflow.SettingSaver.Properties;

namespace Carflow.SettingSaver
{
    public class SettingsSave
    {
        public SettingsSave()
        {
            SetPlant = Settings.Default.SetSelectedPlant;
            SetDeviationDirectory = Settings.Default.SetDirectoryForDeviations;
        }

        public string SetPlant { get; set; }
        public string SetDeviationDirectory { get; set; }

        public void Save()
        {
            Settings.Default.SetDirectoryForDeviations = SetDeviationDirectory;
            Settings.Default.SetSelectedPlant = SetPlant;
            Settings.Default.Save();
        }


    }
}
