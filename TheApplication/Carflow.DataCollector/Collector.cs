﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Carflow.DataObjects;
using SOLIDQBAY;

namespace Carflow.DataCollector
{
    public class Collector
    {
        public delegate ProgressChangedEventHandler OnProgressChanged(ProgressChangedEventArgs progress);

        private static int _processHistoryLinesToDo;
        private static int _historyLinesDone;
        private static readonly int MaxThreadsAllowed = 10;
        private static int _progress;
        public static event ProgressChangedEventHandler ProgressChanged;

        /// <summary>
        ///     this method reports out the progress of reading the data
        /// </summary>
        private static void ReportProgress()
        {
            var temp = (int)(100.0 / _processHistoryLinesToDo * _historyLinesDone);
            temp = Math.Max(temp, 0);
            temp = Math.Min(temp, 100);
            if (temp == _progress) return;

            _progress = temp;

            OnprogressChanged(new ProgressChangedEventArgs(_progress, null));
        }

        /// <summary>
        ///     method that gets all the data from the cars aysynchronic.
        /// </summary>
        /// <param name="cars">List of cars where you need the data from</param>
        /// <param name="token">the cancellation token to cancel the operation</param>
        /// <param name="textparameter">boolian if you want to have the textparameters read out</param>
        /// <param name="numberparameter">boolian if you want to have the numberparameters read out</param>
        /// <param name="eplabel">boolian if you want to have the EPlabel read out.</param>
        /// <returns></returns>
        public static async Task<List<Car>> GetCarDataAsync(List<string> cars, CancellationToken token, bool textparameter, bool numberparameter, bool eplabel)
        {
            if (token.IsCancellationRequested) return null;

            return await Task.Run(() => GetCarData(cars, token, textparameter, numberparameter, eplabel), token);
        }
        public static async Task<List<Car>> GetCarDataAsync(List<string> cars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return null;

            return await Task.Run(() => GetCarData(cars, token, false, false, false), token);
        }
        /// <summary>
        ///     method that gets all the data from the cars.
        /// </summary>
        /// <param name="cars">List of cars where you need the data from</param>
        /// <param name="token">the cancellation token to cancel the operation</param>
        /// <param name="textparameter">boolian if you want to have the textparameters read out</param>
        /// <param name="numberparameter">boolian if you want to have the numberparameters read out</param>
        /// <param name="eplabel">boolian if you want to have the EPlabel read out.</param>
        /// <returns></returns>
        public static List<Car> GetCarData(List<string> cars, CancellationToken token, bool textparameter, bool numberparameter, bool eplabel)
        {
            //set up the amount of lines to do. we start with 2*the car amount because of historyrequest and faultcoderequest
            _processHistoryLinesToDo = 2 * cars.Count;
            if (textparameter) _processHistoryLinesToDo += cars.Count;
            if (numberparameter) _processHistoryLinesToDo += cars.Count;
            if (eplabel) _processHistoryLinesToDo += cars.Count;

            _historyLinesDone = 0;
            ReportProgress();

            //create a list of car-type
            var carsList = new List<Car>(cars.Count);
            foreach (var car in cars)
            {
                carsList.Add(new Car { Mixnr = car });
            }

            GetprocessHistoryMultiThreading(carsList, token);
            GetFaultCodesMultithreading(carsList, token);
            GetProcessDetailMultiThreading(carsList, token);
            GetTerminateItems(carsList, token);
            if (textparameter) GetTextParametersMultiThreading(carsList, token);
            if (numberparameter) GetNumberParametersMultithreading(carsList, token);
            if (eplabel) GetEplabelMultithreading(carsList, token);


            return carsList;
        }

        public static List<Car> GetCarData(List<string> cars, CancellationToken token)
        {
            return GetCarData(cars, token, false, false, false);
        }

        /// <summary>
        ///     method that gets all EPlabels of the cars.
        /// </summary>
        /// <param name="baseCars">List of cars where you need the data from</param>
        /// <param name="token">the cancellation token to cancel the operation</param>
        private static void GetEplabelMultithreading(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //calc the max amount of threads.
            var maxthread = baseCars.Count < MaxThreadsAllowed ? baseCars.Count : MaxThreadsAllowed;

            //calc the cars per thread.
            var carPerThread = baseCars.Count / maxthread;

            //start the threads
            var taskList = new List<Task>();
            for (var i = 0; i < maxthread; i++)
            {
                var lengte = i == maxthread - 1 ? baseCars.Count - i * carPerThread : carPerThread;
                var baseCarsRange = i * carPerThread;

                taskList.Add(Task.Run(() =>
                {
                    for (var j = baseCarsRange; j < baseCarsRange + lengte; j++)
                    {
                        baseCars[j].EPlabel = new QbayBuildCode(baseCars[j].Mixnr);

                        //one more done
                        _historyLinesDone++;
                        ReportProgress();

                        if (token.IsCancellationRequested) return;

                    }
                }, token));
            }

            Task.WaitAll(taskList.ToArray());
        }

        /// <summary>
        ///     method that gets all the numericparameters data of the cars.
        /// </summary>
        /// <param name="baseCars">List of cars where you need the data from</param>
        /// <param name="token">the cancellation token to cancel the operation</param>
        private static void GetNumberParametersMultithreading(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //calc the max amount of threads.
            var maxthread = baseCars.Count < MaxThreadsAllowed ? baseCars.Count : MaxThreadsAllowed;

            //calc the cars per thread.
            var carPerThread = baseCars.Count / maxthread;

            //start the threads
            var taskList = new List<Task>();
            for (var i = 0; i < maxthread; i++)
            {
                var lengte = i == maxthread - 1 ? baseCars.Count - i * carPerThread : carPerThread;
                var baseCarsRange = i * carPerThread;

                taskList.Add(Task.Run(() =>
                {
                    for (var j = baseCarsRange; j < baseCarsRange + lengte; j++)
                    {
                        baseCars[j].NumericParameters = new QbayNumericParameters(baseCars[j].Mixnr).Lines;

                        //one more done
                        _historyLinesDone++;
                        ReportProgress();

                        if (token.IsCancellationRequested) return;

                    }
                }, token));
            }

            Task.WaitAll(taskList.ToArray());
        }

        /// <summary>
        ///     method that gets all the text parameters data of the cars.
        /// </summary>
        /// <param name="baseCars">List of cars where you need the data from</param>
        /// <param name="token">the cancellation token to cancel the operation</param>
        private static void GetTextParametersMultiThreading(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //calc the max amount of threads.
            var maxthread = baseCars.Count < MaxThreadsAllowed ? baseCars.Count : MaxThreadsAllowed;

            //calc the cars per thread.
            var carPerThread = baseCars.Count / maxthread;

            //start the threads
            var taskList = new List<Task>();
            for (var i = 0; i < maxthread; i++)
            {
                var lengte = i == maxthread - 1 ? baseCars.Count - i * carPerThread : carPerThread;
                var baseCarsRange = i * carPerThread;

                taskList.Add(Task.Run(() =>
                {
                    for (var j = baseCarsRange; j < baseCarsRange + lengte; j++)
                    {
                        baseCars[j].TextParameters = new QbayTextParameters(baseCars[j].Mixnr).Lines;

                        //one more done
                        _historyLinesDone++;
                        ReportProgress();

                        if (token.IsCancellationRequested) return;

                    }
                }, token));
            }

            Task.WaitAll(taskList.ToArray());
        }



        /// <summary>
        ///     this method gets all the processdetails of the processes.
        /// </summary>
        /// <param name="baseCars"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private static void GetProcessDetail(List<Car> baseCars, CancellationToken token)
        {
            for (var i = 0; i < baseCars.Count; i++)
            {
                if (baseCars[i].ProcessHistory == null) continue;

                for (var j = 0; j < baseCars[i].ProcessHistory.Count; j++)
                {
                    if (token.IsCancellationRequested) return;

                    var key = baseCars[i].ProcessHistory[j].ProcessHref;

                    if (key == null)
                    {
                        continue;
                    }
                    baseCars[i].TestDetails.Add(key, new QbayTestDetails(key));

                    //one more done
                    _historyLinesDone++;
                    ReportProgress();
                }
            }
        }
        private static void GetProcessDetailMultiThreading(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //calc the max amount of threads.
            var maxthread = baseCars.Count < MaxThreadsAllowed ? baseCars.Count : MaxThreadsAllowed;

            //calc the cars per thread.
            var carPerThread = baseCars.Count / maxthread;

            //start the threads
            var taskList = new List<Task>();
            for (var index1 = 0; index1 < maxthread; index1++)
            {
                var lengte = index1 == maxthread - 1 ? baseCars.Count - index1 * carPerThread : carPerThread;
                var baseCarsRange = index1 * carPerThread;

                taskList.Add(Task.Run(() =>
                {
                    for (var z = baseCarsRange; z < baseCarsRange + lengte; z++)
                    {
                        if (baseCars[z].ProcessHistory == null) continue;

                        for (var j = 0; j < baseCars[z].ProcessHistory.Count; j++)
                        {
                            if (token.IsCancellationRequested) return;

                            var key = baseCars[z].ProcessHistory[j].ProcessHref;

                            if (key == null)
                            {
                                continue;
                            }

                            baseCars[z].TestDetails.Add(key, new QbayTestDetails(key));

                            //one more done
                            _historyLinesDone++;
                            ReportProgress();
                        }

                        if (token.IsCancellationRequested) return;

                    }
                }, token));
            }

            Task.WaitAll(taskList.ToArray());
        }


        /// <summary>
        ///     this method get all the faultcodes of the cars. This is the page holding all the faultcodes for all the processes.
        /// </summary>
        /// <param name="baseCars">the list of cars</param>
        /// <param name="token">the cancellation token</param>
        /// <returns></returns>
        private static void GetFaultCodes(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;


            for (var i = 0; i < baseCars.Count; i++)
            {
                baseCars[i].FaultCodes = new QbayFaultCodesByVin(baseCars[i].Mixnr).Lines;

                //one more done
                _historyLinesDone++;
                ReportProgress();

                if (token.IsCancellationRequested) return;
            }
        }
        private static void GetFaultCodesMultithreading(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //calc the max amount of threads.
            var maxthread = baseCars.Count < MaxThreadsAllowed ? baseCars.Count : MaxThreadsAllowed;

            //calc the cars per thread.
            var carPerThread = baseCars.Count / maxthread;

            //start the threads
            var taskList = new List<Task>();
            for (var i = 0; i < maxthread; i++)
            {
                var lengte = i == maxthread - 1 ? baseCars.Count - i * carPerThread : carPerThread;
                var baseCarsRange = i * carPerThread;

                taskList.Add(Task.Run(() =>
                {
                    for (var j = baseCarsRange; j < baseCarsRange + lengte; j++)
                    {
                        baseCars[j].FaultCodes = new QbayFaultCodesByVin(baseCars[j].Mixnr).Lines;

                        //one more done
                        _historyLinesDone++;
                        ReportProgress();

                        if (token.IsCancellationRequested) return;

                    }
                }, token));
            }

            Task.WaitAll(taskList.ToArray());
        }



        /// <summary>
        ///     this method gets the process history of a baselist of cars and fills in the detail in the list.
        ///     There is no splitup so that this could even go faster. There is only one thread for this task.
        /// </summary>
        /// <param name="baseCars">the list of cars.</param>
        /// <param name="token">the cancellation token</param>
        private static void GetProcessHistory(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            for (var i = 0; i < baseCars.Count; i++)
            {
                baseCars[i].ProcessHistory = new QbayProcessHistory(baseCars[i].Mixnr).Lines;

                if (token.IsCancellationRequested) return;

                //add the amount of lines to the total
                _processHistoryLinesToDo += baseCars[i].ProcessHistory.Count;

                //one more done
                _historyLinesDone++;
                ReportProgress();
            }

        }
        private static void GetprocessHistoryMultiThreading(List<Car> baseCars, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //calc the max amount of threads.
            var maxthread = baseCars.Count < MaxThreadsAllowed ? baseCars.Count : MaxThreadsAllowed;

            //calc the cars per thread.
            var carPerThread = baseCars.Count / maxthread;

            //start the threads
            var taskList = new List<Task>();
            for (var i = 0; i < maxthread; i++)
            {
                var lengte = i == maxthread - 1 ? baseCars.Count - i * carPerThread : carPerThread;
                var baseCarsRange = i * carPerThread;

                taskList.Add(Task.Run(() =>
                {
                    for (var j = baseCarsRange; j < baseCarsRange + lengte; j++)
                    {
                        baseCars[j].ProcessHistory = new QbayProcessHistory(baseCars[j].Mixnr).Lines;

                        if (token.IsCancellationRequested) return;

                        //add the amount of lines to the total
                        _processHistoryLinesToDo += baseCars[j].ProcessHistory.Count;

                        //one more done
                        _historyLinesDone++;
                        ReportProgress();

                    }
                }, token));
            }

            Task.WaitAll(taskList.ToArray());
        }




        /// <summary>
        /// </summary>
        /// <param name="baseCars"></param>
        /// <param name="start"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        private static int GetAmountOfCars(List<Car> baseCars, int start, int threshold)
        {
            var total = 0;
            var cars = 0;

            while (total < threshold)
            {
                try
                {
                    if (baseCars.Count == start + cars)
                    {
                        break;
                    }

                    total += baseCars[start + cars].ProcessHistory.Count;
                    cars++;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return cars;
        }

        /// <summary>
        ///     this method add the terminale issues to the faultcodelist since this is not included.
        /// </summary>
        /// <param name="baseCars"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private static void GetTerminateItems(List<Car> baseCars, CancellationToken token)
        {
            foreach (var baseCar in baseCars)
            {
                var i = 0;
                foreach (var detailpage in baseCar.TestDetails)
                {
                    foreach (var detail in detailpage.Value.Lines)
                    {
                        if (detail.Status.ToUpper() == "TERMINATE")
                        {
                            //get the faultcodes connected to this detail
                            var foutcodes = new QbayFaultCodesByTest(detail.TestDetailsHref);
                            //foreach faultcode create a faultcodebyvin line
                            foreach (var code in foutcodes.Lines)
                            {
                                if (code.Faultcode == "ID001") continue;
                                var t = new QbayFaultCodesByVinLine
                                {
                                    Process = baseCar.ProcessHistory[i].Process,
                                    Faultcodes = code.Faultcode,
                                    Faultlabel = code.Label,
                                    Status = "Terminate",
                                    TestName = detail.TestDetails,
                                    TestTime = detail.TestTime,
                                    Tester = detail.Tester
                                };

                                //add these faultcodebyvin line to the faulcode list
                                baseCar.FaultCodes.Add(t);
                            }
                        }
                    }
                    i++;
                }

                //order the faultcode list again.
                baseCar.FaultCodes = baseCar.FaultCodes.OrderBy(o => o.TestTime).ToList();
            }
        }

        /// <summary>
        ///     Method to throw the event.
        /// </summary>
        /// <param name="e"></param>
        private static void OnprogressChanged(ProgressChangedEventArgs e)
        {
            var handler = ProgressChanged;
            if (handler != null) handler(null, e);
        }
    }
}
