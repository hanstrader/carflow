﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Carflow.DataCollector;
using Carflow.DataObjects;
using Carflow.Deviations;
using Carflow.TableBuilder;
using Carflow.Xlist;
using SOLIDQBAY;

namespace Carflow.Business
{
    public class Carflow : BaseNotify, ICarflowDataCollector
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        private DataTable _allRemainingIssueDataTable;
        private DataTable _allRunDataTable;
        private DataTable _firstRunDataTable;
        private List<string[]> _allremainingIssueMatrix;
        private List<string[]> _allRunMatrix;
        private List<string[]> _firstRunMatrix;
        private bool _busy;
        private List<Car> _cars;
        private Car _currentCar;
        private List<Deviatie> _deviaties;
        private string _hrefToShow;
        private int _indexCurrentCar;
        private int _progress;
        private string _selectedPlant;


        public Carflow()
        {
            _cancellationTokenSource = new CancellationTokenSource();
        }

        #region properties

        public DataTable FirstRunDataTable
        {
            get { return _firstRunDataTable; }
            set
            {
                _firstRunDataTable = value;
                OnPropertyChanged();
            }
        }

        public DataTable AllRunDataTable

        {
            get { return _allRunDataTable; }
            set
            {
                _allRunDataTable = value;
                OnPropertyChanged();
            }
        }

        public DataTable AllRemainingIssueDataTable
        {
            get { return _allRemainingIssueDataTable; }
            set
            {
                _allRemainingIssueDataTable = value;
                OnPropertyChanged();
            }
        }

        public string HrefToShow
        {
            get { return _hrefToShow; }
            set
            {
                _hrefToShow = value;
                OnPropertyChanged();
            }
        }

        public List<Deviatie> Deviaties
        {
            get { return _deviaties; }
            set
            {
                if (_deviaties != value)
                {
                    _deviaties = value;
                    OnPropertyChanged();
                    UpdateXlists();
                }
            }
        }


        public Car CurrentCar
        {
            private set
            {
                _currentCar = value;
                OnPropertyChanged();
            }
            get { return _currentCar; }
        }


        public int Progress
        {
            get { return _progress; }
            set
            {
                _progress = value;
                OnPropertyChanged();
            }
        }


        public string SelectedPlant
        {
            get { return _selectedPlant; }
            set
            {
                if (QbayPlant.Plant == value)
                {
                    return;
                }

                QbayPlant.Plant = value;
                _selectedPlant = value;
                OnPropertyChanged();
            }
        }


        public List<string> PossiblePlants
        {
            get
            {
                var possibleList = new List<string>();
                foreach (var plant in QbayPlant.PlantsPossible)
                {
                    possibleList.Add(plant.Substring(0, plant.IndexOf(';')));
                }
                return possibleList;
            }
        }


        public List<string[]> AllRemainingIssuesMatrix
        {
            get { return _allremainingIssueMatrix; }
            set
            {
                _allremainingIssueMatrix = value;
                AllRemainingIssueDataTable = MatrixToTable(value, "Remaining issues");
                OnPropertyChanged();
            }
        }

        public List<string[]> FirstRunMatrix
        {
            get { return _firstRunMatrix; }
            set
            {
                _firstRunMatrix = value;
                FirstRunDataTable = MatrixToTable(value, "First run");
                OnPropertyChanged();
            }
        }

        public List<string[]> AllRunMatrix
        {
            get { return _allRunMatrix; }
            set
            {
                _allRunMatrix = value;
                AllRunDataTable = MatrixToTable(value, "All runs");
                OnPropertyChanged();
            }
        }

        public bool Busy
        {
            get { return _busy; }
            set
            {
                _busy = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region methods

        /// <summary>
        /// This methode collects all the carinformation from QBAY in an asynchronic way.
        /// </summary>
        /// <param name="mixnrs"></param>
        /// <returns></returns>
        public async Task GetDataAsync(List<string> mixnrs)
        {
            //preparation for busy, cancel and subscribing to event.
            Busy = true;
            var token = _cancellationTokenSource.Token;
            Collector.ProgressChanged += Collector_ProgressChanged;

            //collect all the cardata
            _cars = await GetCarDataAsync(mixnrs, token);

            //update all the matrices.
            UpdateXlists();

            //set the current car to the first car.
            _indexCurrentCar = 0;
            CurrentCar = _cars[_indexCurrentCar];

            //release the preparation
            Collector.ProgressChanged -= Collector_ProgressChanged;
            Progress = 0;
            Busy = false;
        }

        /// <summary>
        /// This methode collects all the carinformation from QBAY.
        /// </summary>
        /// <param name="mixnrs"></param>
        public void GetData(List<string> mixnrs, bool textparameters, bool numericparameters, bool eplabel)
        {
            //preparation for busy, cancel and subscribing to event.
            Busy = true;
            var token = _cancellationTokenSource.Token;
            Collector.ProgressChanged += Collector_ProgressChanged;

            //collect all the cardata asynchronic
            _cars = GetCarData(mixnrs, token, true, true, false);

            //update all the matrices.
            UpdateXlists();

            //set the current car to the first car.
            _indexCurrentCar = 0;
            CurrentCar = _cars[_indexCurrentCar];

            //release the preparation
            Collector.ProgressChanged -= Collector_ProgressChanged;
            Progress = 0;
            Busy = false;
        }

        public void NextCar()
        {
            if (_indexCurrentCar < _cars.Count - 1)
            {
                _indexCurrentCar++;
                CurrentCar = _cars[_indexCurrentCar];
            }
        }

        public void PreviousCar()
        {
            if (_indexCurrentCar > 0)
            {
                _indexCurrentCar--;
                CurrentCar = _cars[_indexCurrentCar];
            }
        }

        public void Cancel()
        {
            if (Busy)
            {
                _cancellationTokenSource.Cancel();
            }
        }

        public void SetCurrentCarToMix(string mixnr)
        {
            var index = _cars.FindIndex(o => o.Mixnr == mixnr);
            var current = _cars.FindIndex(o => CurrentCar.Mixnr == o.Mixnr);

            if (index >= 0 && (index != current))
            {
                _indexCurrentCar = index;
                CurrentCar = _cars[_indexCurrentCar];
            }
        }

        public void ShowWebProcess(int orderOfProcess)
        {
            HrefToShow = CurrentCar.ProcessHistory[orderOfProcess].ProcessWebLink;
        }


        private static async Task<List<Car>> GetCarDataAsync(List<string> mixnrs, CancellationToken token)
        {
            var carsList = await Collector.GetCarDataAsync(mixnrs, token);


            //create the matrix for each car (as this only take 7 ms this should not be made async)
            for (var i = 0; i < carsList.Count; i++)
            {
                carsList[i].Matrix = MainMatrix.Create(carsList[i]);
                carsList[i].RemainingIssueMatrix = RemainingIssuesMatrix.Create(carsList[i]);
            }

            return carsList;
        }

        private static List<Car> GetCarData(List<string> mixnrs, CancellationToken token, bool textparameter, bool numberparameter, bool eplabel)
        {
            var carsList = Collector.GetCarData(mixnrs, token, textparameter, numberparameter, eplabel);

            //create the matrix for each car (as this only take 7 ms this should not be made async)
            for (var i = 0; i < carsList.Count; i++)
            {
                carsList[i].Matrix = MainMatrix.Create(carsList[i]);
                carsList[i].RemainingIssueMatrix = RemainingIssuesMatrix.Create(carsList[i]);
            }

            return carsList;
        }

        private void Collector_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Progress = e.ProgressPercentage;
        }

        private DataTable MatrixToTable(List<string[]> source, string tableName)
        {
            var table = new DataTable();
            table.TableName = tableName;

            for (var index = 0; index < source[0].Length; index++)
            {
                var column = table.Columns.Add();
                column.MaxLength = 999;
                column.ColumnName = source[0][index];
            }


            if (source.Count == 1) return table;

            for (var i = 1; i < source.Count; i++)
            {
                table.Rows.Add(source[i]);
            }


            return table;
        }

        private void UpdateXlists()
        {
            AllRemainingIssuesMatrix = XList.OpenIssues(_cars, Deviaties);
            AllRunMatrix = XList.AllRuns(_cars, Deviaties);
            FirstRunMatrix = XList.FirstRuns(_cars, Deviaties);
        }



        #endregion
    }
}
