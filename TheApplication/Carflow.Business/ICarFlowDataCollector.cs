﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carflow.DataObjects;

namespace Carflow.Business
{
    public interface ICarflowDataCollector
    {
        Car CurrentCar { get; }
        int Progress { get; set; }
        string SelectedPlant { get; set; }
        List<string> PossiblePlants { get; }
        List<string[]> AllRemainingIssuesMatrix { get; set; }
        List<string[]> FirstRunMatrix { get; set; }
        List<string[]> AllRunMatrix { get; set; }
        bool Busy { get; set; }
        event PropertyChangedEventHandler PropertyChanged;
        Task GetDataAsync(List<string> mixnrs);
        void NextCar();
        void PreviousCar();

        void Cancel();
    }
}
