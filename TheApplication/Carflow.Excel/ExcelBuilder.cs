﻿using System;
using System.Collections.Generic;
using System.Data;
using ClosedXML.Excel;
using System.IO;

namespace Carflow.Excel
{
    public class ExcelBuilder
    {
        /// <summary>
        ///     all the reports will be stored here.
        /// </summary>
        private Dictionary<string, Object> _reports;

        /// <summary>
        ///     the location where the excel file should be written
        /// </summary>
        public string FileLocation { get; set; }

        /// <summary>
        ///     the name of the excel file
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        ///     constructor
        /// </summary>
        public ExcelBuilder()
        {
            _reports = new Dictionary<string, object>();
        }

        /// <summary>
        ///     This clears all the table currently held in the builder.
        /// </summary>
        public void Clear()
        {
            _reports.Clear();
        }


        /// <summary>
        ///    Adds a table to the excelbuilder that will be reprensented as a worksheet.
        /// </summary>
        /// <param name="name">the name the worksheet will get</param>
        /// <param name="table">the table that the worksheet will present</param>
        public void Add(string name, DataTable table)
        {
            _reports.Add(name,table);
        }

        /// <summary>
        ///     adds a list of string[] to the excelbuilder that will be represented as a worksheet.
        /// </summary>
        /// <param name="name">the name the worksheet will get</param>
        /// <param name="table">the table that the worksheet will present</param>
        public void Add(string name, List<string[]> table)
        {
            _reports.Add(name,table);
        }

        /// <summary>
        ///     Creates the excel file of all the tables present in the bulder.
        /// </summary>
        /// <param name="overwrite"></param>
        public void Create(bool overwrite)
        {
            var workbook = new XLWorkbook();

            foreach (KeyValuePair<string, object> report in _reports)
            {
                IXLWorksheet ws;
                switch (report.Value)
                {

                    case DataTable table:
                        table.TableName = report.Key;
                        ws = workbook.Worksheets.Add(table);
                        CreateFromTable(ws);
                        break;

                    case List<string[]> table:
                        ws = workbook.Worksheets.Add(report.Key);
                        CreateFromList(ws, table);
                        break;

                    default:
                        break;

                    case null:
                        break;
  
                }
            }
            
            workbook.SaveAs(Path.Combine(FileLocation,FileName));
        }

        private void CreateFromList(IXLWorksheet ws, List<string[]> table)
        {
            //here you have to add the the list and the styling.

            AddStyling(ws);
        }

        private void CreateFromTable(IXLWorksheet ws)
        {
            AddStyling(ws);
        }

        private void AddStyling(IXLWorksheet ws)
        {
            var usedRange = ws.RangeUsed();

            //general styling
            ws.Style.Font.FontName = "Calibri";
            ws.Style.Font.FontSize = 8;
            ws.Columns(1,5).AdjustToContents();
            ws.Columns(6, usedRange.ColumnCount() - 1).Width = 2;
            ws.Columns(6, usedRange.ColumnCount() - 1).Style.Alignment.Horizontal =
                XLAlignmentHorizontalValues.Center;
            ws.Column(usedRange.ColumnCount()).AdjustToContents();
            ws.Rows().Height = 11.5;
            ws.Row(1).Height = 54;
            ws.SheetView.Freeze(1,5);

            //style the header.
            var headerRange = usedRange.Row(1);
            headerRange.Style.Fill.BackgroundColor = XLColor.Green;

            var mixnRange = headerRange.Cells(6, usedRange.ColumnCount() -1);
            mixnRange.Style.Alignment.SetTextRotation(-90);
            mixnRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
            
            //colour the DTC's
            for (int i = 2; i < usedRange.RowCount(); i++)
            {
                if (!ws.Cell(i,3).GetValue<string>().StartsWith("V") && !ws.Cell(i,3).GetValue<string>().StartsWith("S"))
                {
                    ws.Cell(i,3).Style.Font.FontColor = XLColor.Blue;
                    ws.Cell(i, 4).Style.Font.FontColor = XLColor.Blue;
                }
            }


           

        }



    }
}
