﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carflow.Deviations
{
    interface IWriterReader
    {
        Task<System.Collections.ObjectModel.ObservableCollection<Deviatie>> ReadAsync(string file);
        System.Collections.ObjectModel.ObservableCollection<Deviatie> Read(string file);

        void WriteAsync(string file, ObservableCollection<Deviatie> deviatiesObservableCollection);
        void Write(string file, ObservableCollection<Deviatie> deviatiesObservableCollection);
    }
}
