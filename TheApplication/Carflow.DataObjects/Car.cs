﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carflow.DataObjects
{
    public class Car : CarData
    {
        private List<string[]> _matrix;
        private List<string[]> _remainingIssueMatrix;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mixnr">Mixnr of the car</param>
        public Car(string mixnr)
        {
            Mixnr = mixnr;
        }

        /// <summary>
        /// default constructor
        /// </summary>
        public Car()
        {

        }

        /// <summary>
        /// Copies the Hrefline from the processhistory to the testdetails.
        /// </summary>
        internal void CopyHistoryHrefToDetails()
        {
            foreach (var historyline in ProcessHistory)
            {
                TestDetails.Add(historyline.ProcessHref, null);
            }
        }


        /// <summary>
        /// the matrix that is displayed by carflow.
        /// </summary>
        public List<string[]> Matrix
        {
            get { return _matrix; }
            set
            {
                _matrix = value;
                MatrixDataTable = MatrixToTable(value);
            }
        }

        /// <summary>
        /// the matrix that is displayed by carflow.
        /// </summary>
        public List<string[]> RemainingIssueMatrix
        {
            get { return _remainingIssueMatrix; }
            set
            {
                _remainingIssueMatrix = value;
                RemainIssueDataTable = MatrixToTable(value);
            }
        }

        /// <summary>
        /// The datatable representing the MainMatrix
        /// </summary>
        public DataTable MatrixDataTable { get; set; }

        /// <summary>
        /// The datatable representing the remaining issues.
        /// </summary>
        public DataTable RemainIssueDataTable { get; set; }


        /// <summary>
        /// method that creates a table based on a matrix.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private DataTable MatrixToTable(List<string[]> source)
        {
            if (source == null) return null;

            DataTable table = new DataTable();

            //add columns
            for (int index = 0; index < source[0].Length; index++)
            {
                DataColumn column = table.Columns.Add();
                column.MaxLength = 999;
                try
                {
                    column.ColumnName = source[0][index].Replace("(", "t = ").Replace(")", " sec").Replace("/", "-");
                }
                catch
                {
                    column.ColumnName = source[0][index].Replace("(", "t = ").Replace(")", " sec").Replace("/", "-") + "\r\n duplicate";
                }
            }

            if (source.Count == 1) return table;

            //add the rows
            for (int i = 1; i < source.Count; i++)
            {
                var rij = table.Rows.Add(source[i]);
            }



            return table;
        }


    }
}
