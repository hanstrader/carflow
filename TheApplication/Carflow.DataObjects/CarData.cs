﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SOLIDQBAY;

namespace Carflow.DataObjects
{
    public abstract class CarData : BaseNotify
    {
        private List<QbayProcessHistoryLine> _processHistory;
        private Dictionary<string, QbayTestDetails> _testDetails = new Dictionary<string, QbayTestDetails>();
        private List<QbayFaultCodesByVinLine> _faultCodes;

        /// <summary>
        /// this value holds the total number of NOK fields. This can be used for loadbalancing if we use multithreading for loading the data.
        /// </summary>
        internal int TotalNoKfields { get; set; }

        /// <summary>
        /// Mixnr of the car
        /// </summary>
        public string Mixnr { get; set; }

        /// <summary>
        /// the processhistory lines for this mixnr. the processes will be ordererd.
        /// </summary>
        public List<QbayProcessHistoryLine> ProcessHistory
        {
            get { return _processHistory; }
            set
            {
                _processHistory = value;
                _processHistory = _processHistory.OrderBy(o => o.Starttime).ToList();
            }
        }

        /// <summary>
        /// the testdetails for the mixnr
        /// </summary>
        public Dictionary<string, QbayTestDetails> TestDetails
        {
            get { return _testDetails; }
            set
            {
                _testDetails = value;

                TotalNoKfields = GetTotalNokFields();
            }
        }

        /// <summary>
        /// All the faultcodes of this car. The list will be ordered.
        /// </summary>
        public List<QbayFaultCodesByVinLine> FaultCodes
        {
            get { return _faultCodes; }
            set
            {
                _faultCodes = value;
                _faultCodes = FaultCodes.OrderBy(o => o.TestTime).ToList();
            }
        }

        /// <summary>
        /// All the numericparameters of this car. 
        /// </summary>
        public List<QbayNumericParametersLine> NumericParameters { get; set; }

        /// <summary>
        /// All the textparameter of this car.
        /// </summary>
        public List<QbayTextParametersLine> TextParameters { get; set; }

        /// <summary>
        /// All the buldcodes of this car.
        /// </summary>
        public QbayBuildCode EPlabel { get; set; }

        /// <summary>
        /// calculate  the totalNOKfields in the complete testdetailsList. This can be used for load balancing.
        /// </summary>
        /// <returns></returns>
        internal int GetTotalNokFields()
        {
            int amountNok = 0;

            foreach (var detailpage in TestDetails.Values)
            {
                foreach (var test in detailpage.Lines)
                {
                    if (test.Status != "OK") amountNok++;
                }
            }
            return amountNok;
        }

        /// <summary>
        /// constructor
        /// </summary>
        protected CarData()
        {

        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mixnr">Mixnr of the car</param>
        protected CarData(string mixnr)
        {
            Mixnr = mixnr;
        }

    }
}
