﻿using System.Threading.Tasks;
using System.Windows.Input;
using Carflow.Deviations;
using Carflow.SettingSaver;
using MixnrGetter;

namespace Carflow.UI.ViewModel
{
    public partial class ViewModel : Base
    {
        private readonly SettingsSave _settings;
        //private Cleaner _cleaner;
        private ICommand _getCarDataCommand;
        private string _inputText;
        private Getter _mixnrGetter;
        private ICommand _nextCarCommand;
        private ICommand _previousCarCommand;
        private Business.Carflow _qbay;
        //private RemarkControler _remarkControler;
        private ICommand _saveSettingsCommand;
        private ICommand _selectedItemChangedCommand;
        private ICommand _updateMixnrsCommand;


        public ViewModel()
        {
            _qbay = new Business.Carflow();
            _deviationControler = new DeviationControler();
            //_remarkControler = new RemarkControler();
            //_cleaner = new Cleaner();
            _mixnrGetter = new Getter();
            _deviationControler.PropertyChanged += _deviationControler_PropertyChanged;
            _settings = new SettingsSave();

            _qbay.SelectedPlant = _settings.SetPlant;
            _deviationControler.FileLocation = _settings.SetDeviationDirectory;
        }


        public Getter MixnrsGetter
        {
            get => _mixnrGetter;
            set
            {
                if (value == _mixnrGetter) return;
                _mixnrGetter = value;
                OnPropertyChanged();
            }
        }

        public ICommand UpdateMixnrCommand
        {
            get
            {
                if (_updateMixnrsCommand == null)
                    _updateMixnrsCommand = new RelayCommand(o => GetMixnr(), o => InputText != null);
                return _updateMixnrsCommand;
            }
        }

        public string InputText
        {
            get => _inputText;
            set
            {
                _inputText = value;
                OnPropertyChanged();
            }
        }


        public Business.Carflow Qbay
        {
            get => _qbay;
            set
            {
                if (value == _qbay)
                    return;
                _qbay = value;
                OnPropertyChanged();
            }
        }

        public ICommand GetCarDataCommand
        {
            get
            {
                if (_getCarDataCommand == null)
                    _getCarDataCommand = new RelayCommand(o => GetCarData(),
                        o =>
                            MixnrsGetter.Mixnrs.Length > 0 && Qbay.PossiblePlants.Contains(Qbay.SelectedPlant) &&
                            !Qbay.Busy);
                return _getCarDataCommand;
            }
        }

        public ICommand NextCarCommand
        {
            get
            {
                if (_nextCarCommand == null)
                    _nextCarCommand = new RelayCommand(o => NextCar(), o => !Qbay.Busy && Qbay.CurrentCar != null);
                return _nextCarCommand;
            }
        }

        public ICommand PreviousCarCommand
        {
            get
            {
                if (_previousCarCommand == null)
                    _previousCarCommand = new RelayCommand(o => PreviousCar(),
                        o => !Qbay.Busy && Qbay.CurrentCar != null);
                return _previousCarCommand;
            }
        }

        public ICommand SelectedItemChangedCommand
        {
            get
            {
                if (_selectedItemChangedCommand == null)
                    _selectedItemChangedCommand =
                        new DelegateCommand(SelectedItemChanged, o => Qbay.CurrentCar != null);
                return _selectedItemChangedCommand;
            }
        }

        public ICommand SaveSettingsCommand
        {
            get
            {
                if (_saveSettingsCommand == null)
                    _saveSettingsCommand = new DelegateCommand(o => SaveSettings(), o => true);
                return _saveSettingsCommand;
            }
        }

        private void GetMixnr()
        {
            MixnrsGetter = new Getter(InputText);
        }

        private async void GetCarData()
        {
            var taak = Task.Factory.StartNew(() => Qbay.GetData(MixnrsGetter.MixnrList, false, false, false));
            await taak;

            //await Qbay.GetDataAsync(MixnrsGetter.MixnrList);
        }

        private void NextCar()
        {
            Qbay.NextCar();
        }

        private void PreviousCar()
        {
            Qbay.PreviousCar();
        }

        private void SelectedItemChanged(object parameter)
        {
            var mixnr = parameter as string;

            if (mixnr == Qbay.CurrentCar.Mixnr) return;

            Qbay.SetCurrentCarToMix(mixnr);
        }

        private void SaveSettings()
        {
            _settings.SetDeviationDirectory = DeviationControler.FileLocation;
            _settings.SetPlant = Qbay.SelectedPlant;
            _settings.Save();
        }
    }
}