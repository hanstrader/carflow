﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Input;
using Carflow.Deviations;


namespace Carflow.UI.ViewModel
{
    partial class ViewModel
    {
        private ICommand _addDeviationCommand;
        private ICommand _deleteDeviationCommand;
        private DeviationControler _deviationControler;
        private ICommand _setDirectoryCommand;
        private bool _shouldUseDeviaties;

        public DeviationControler DeviationControler
        {
            get => _deviationControler;
            set
            {
                if (_deviationControler == value) return;

                _deviationControler = value;
                OnPropertyChanged();
            }
        }

        public ICommand SetDirectoryCommand
        {
            get
            {
                if (_setDirectoryCommand == null)
                    _setDirectoryCommand = new DelegateCommand(o => SetDirectory(), o => true);
                return _setDirectoryCommand;
            }
        }

        public ICommand AddDeviationCommand
        {
            get
            {
                if (_addDeviationCommand == null)
                    _addDeviationCommand = new RelayCommand(AddDeviation, o => true);
                return _addDeviationCommand;
            }
        }

        public ICommand DeleteDeviationCommand
        {
            get
            {
                if (_deleteDeviationCommand == null)
                    _deleteDeviationCommand = new DelegateCommand(DeleteDeviation, o => true);
                return _deleteDeviationCommand;
            }
        }

        public bool ShouldUseDeviaties
        {
            get => _shouldUseDeviaties;
            set
            {
                _shouldUseDeviaties = value;

                Qbay.Deviaties = value ? DeviationControler.ActiveDeviaties : null;

                OnPropertyChanged();
            }
        }

        public void SetDirectory()
        {
            var folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() != DialogResult.Abort)
                DeviationControler.FileLocation = folderBrowserDialog.SelectedPath;
        }

        private void AddDeviation(object parametersObjects)
        {
            var parameters = parametersObjects as object[];

            var deviatie = new Deviatie
            {
                Comment = parameters[4] as string,
                Project = parameters[0] as string,
                FaultCode = parameters[3] as string,
                Module = parameters[2] as string,
                Serie = parameters[1] as string
            };


            DeviationControler.Add(deviatie);
        }

        private void DeleteDeviation(object parametersObjects)
        {
            var parameters = parametersObjects as object[];

            var deviatie = new Deviatie
            {
                Comment = parameters[4] as string,
                Project = parameters[0] as string,
                FaultCode = parameters[3] as string,
                Module = parameters[2] as string,
                Serie = parameters[1] as string
            };

            DeviationControler.Delete(deviatie);
        }

        private void _deviationControler_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ActiveDeviaties")
                Qbay.Deviaties = ShouldUseDeviaties ? DeviationControler.ActiveDeviaties : null;
        }
    }
}