﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Navigation;
using Carflow.Deviations;

namespace Carflow.UI
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DeviationList.SelectedItem = null;
            TbDevComment.Text = "";
            TbDevFault.Text = "";
            TbDevModule.Text = "";
            TbDevProject.Text = "";
            TbDevSerie.Text = "";
        }

        private void DetailsViewClicked(object sender, RoutedEventArgs e)
        {
            var columnHeader = sender as DataGridColumnHeader;

            if (columnHeader?.DisplayIndex > 3)
            {
                Model.Qbay.ShowWebProcess(columnHeader.DisplayIndex - 4);
                TabControl.SelectedIndex = 1;
            }
        }


        private void BackButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                WebView.GoBack();
            }
            catch
            {
                //ignore
            }
        }

        private void ForwardButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                WebView.GoForward();
            }
            catch
            {
                //ignore
            }
        }


        public void HideScriptErrors(WebBrowser wb, bool hide)
        {
            var fiComWebBrowser =
                typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            var objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null)
            {
                wb.Loaded += (o, s) => HideScriptErrors(wb, hide); //In case we are to early
                return;
            }
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser,
                new object[] {hide});
        }

        private void WebView_OnNavigated(object sender, NavigationEventArgs e)
        {
            HideScriptErrors(WebView, true);
        }
    }

    public class ToDeviationConverter : IMultiValueConverter

    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.Clone();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class FontWeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var text = value.ToString().ToUpper();

            switch (text)
            {
                case "NOK":
                    return Brushes.Orange;
                case "OK":
                    return Brushes.LightGreen;
                case "ABORT":
                case "TERMINATE":
                    return Brushes.Red;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FontColourConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var text = value.ToString().ToUpper();
            if (text == "ABORT" || text == "TERMINATE" || text == "OK" || text == "NOK")
                return Brushes.Black;
            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DeviationToColour : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var gridRow = value as DataGridRow;
            if (gridRow == null)
                return null;

            var rowview = gridRow.DataContext as DataRowView;

            var test = (rowview[1] as string).ToUpper();
            var fault = (rowview[2] as string).ToUpper();

            if (parameter is List<Deviatie> deviaties)
                if (deviaties.Exists(o => test.Contains(o.Module) && fault == o.FaultCode))
                    return Brushes.YellowGreen;


            var text = fault.ToUpper().Substring(0, 1);

            if (text == "S" || text == "V")
                return Brushes.Black;
            return Brushes.Blue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DeviationToColour2 : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            var deviaties = value[0] as List<Deviatie>;
            var showdev = (bool) value[1];
            var gridRow = value[2] as DataGridRow;

            if (gridRow == null)
                return null;

            var rowview = gridRow.DataContext as DataRowView;
            if (rowview == null) return Brushes.Black;

            var test = (rowview[1] as string).ToUpper();
            var fault = (rowview[2] as string).ToUpper();


            if (deviaties != null && showdev)
                if (deviaties.Exists(o => test.Contains(o.Module) && fault == o.FaultCode))
                    return Brushes.LightSteelBlue;

            var text = fault.ToUpper().Substring(0, 1);

            if (text == "S" || text == "V")
                return Brushes.Black;
            return Brushes.Blue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public static class WebBrowserUtility
    {
        public static readonly DependencyProperty BindableSourceProperty =
            DependencyProperty.RegisterAttached("BindableSource", typeof(string), typeof(WebBrowserUtility),
                new UIPropertyMetadata(null, BindableSourcePropertyChanged));

        public static string GetBindableSource(DependencyObject obj)
        {
            return (string) obj.GetValue(BindableSourceProperty);
        }

        public static void SetBindableSource(DependencyObject obj, string value)
        {
            obj.SetValue(BindableSourceProperty, value);
        }

        public static void BindableSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is WebBrowser browser)
            {
                var uri = e.NewValue as string;
                browser.Source = !string.IsNullOrEmpty(uri) ? new Uri(uri) : null;
            }
        }
    }
}